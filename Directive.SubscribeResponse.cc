//==================================================================================================
//    Directive.SubscribeResponse.cc
//==================================================================================================
#include "Directive.SubscribeResponse.h"

namespace Directive
{
  //================================================================================================
  ///   Default constructor.
  //================================================================================================
  SubscribeResponse::SubscribeResponse()
  {
  }

  //================================================================================================
  ///   Useful constructor.
  //================================================================================================
  SubscribeResponse::SubscribeResponse(boost::uint32_t code, const std::vector<std::string>& users)
  : code(code), users(users)
  {
  }

  //================================================================================================
  ///   Destructor.
  //================================================================================================
  SubscribeResponse::~SubscribeResponse()
  {
  }

  //================================================================================================
  ///   Converts to json.
  //================================================================================================
  std::string SubscribeResponse::toJson() const
  {
    json::object o;
    o.append(Directive::Base::IdKey, Directive::asStr(this->id()));
    o.append(Directive::SubscribeResponse::CodeKey, Directive::asStr(this->code));
    json::array a;
    for (std::vector<std::string>::const_iterator i = this->users.begin(); i != this->users.end();
         i++)
    {
      a.append(*i);
    }

    o.append(Directive::SubscribeResponse::UsersKey, a);
    return json::encode(o);
  }

  //================================================================================================
  ///   Converts from json.
  //================================================================================================
  void SubscribeResponse::fromJson(const std::string& data)
  {
    json::value j = json::parse(data);
    if (j.is_object())
    {
      if (!j.has_key(Directive::SubscribeResponse::CodeKey))
      {
        return;
      }

      json::value code = j[Directive::SubscribeResponse::CodeKey];
      if (code.is_string())
      {
        this->code = Directive::asInt(code.to_string());
      }

      json::value users = j[Directive::SubscribeResponse::UsersKey];
      if (users.is_array())
      {
        json::array a(users.to_array());
        for (int i = 0; i < a.size(); i++)
        {
          if (a[i].is_string())
          {
            this->users.push_back(a[i].to_string());
          }
        }
      }
    }
  }

  //================================================================================================
  ///   Returns the id.
  //================================================================================================
  Directive::Id SubscribeResponse::id() const
  {
    return Directive::SubscribeResponseId;
  }

  //================================================================================================
  ///   Returns the response code from the connect response.
  //================================================================================================
  boost::uint32_t SubscribeResponse::getCode() const
  {
    return this->code;
  }

  //================================================================================================
  ///   Returns the list of currently connected users.
  //================================================================================================
  const std::vector<std::string>& SubscribeResponse::getUsers() const
  {
    return this->users;
  }
};
