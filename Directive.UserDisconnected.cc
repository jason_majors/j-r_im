//==================================================================================================
//    Directive.UserDisconnected.cc
//==================================================================================================
#include "Directive.UserDisconnected.h"

namespace Directive
{
  //================================================================================================
  ///   Default constructor.
  //================================================================================================
  UserDisconnected::UserDisconnected()
  {
  }

  //================================================================================================
  ///   Useful constructor.
  //================================================================================================
  UserDisconnected::UserDisconnected(const std::string& name) : name(name)
  {
  }

  //================================================================================================
  ///   Destructor.
  //================================================================================================
  UserDisconnected::~UserDisconnected()
  {
  }

  //================================================================================================
  ///   Converts to json.
  //================================================================================================
  std::string UserDisconnected::toJson() const
  {
    json::object o;
    o.append(Directive::Base::IdKey, Directive::asStr(this->id()));
    o.append(Directive::UserDisconnected::UserDisconnectedKey, this->name);
    return json::encode(o);
  }

  //================================================================================================
  ///   Converts from json.
  //================================================================================================
  void UserDisconnected::fromJson(const std::string& data)
  {
    json::value j = json::parse(data);
    if (j.is_object())
    {
      if (j.has_key(Directive::UserDisconnected::UserDisconnectedKey))
      {
        json::value name = j[Directive::UserDisconnected::UserDisconnectedKey];
        if (name.is_string())
        {
          this->name = name.to_string();
        }
      }
    }
  }

  //================================================================================================
  ///   Returns the id.
  //================================================================================================
  Directive::Id UserDisconnected::id() const
  {
    return Directive::UserDisconnectedId;
  }

  //================================================================================================
  ///   Returns the user name contained in this User Disconnected message.
  //================================================================================================
  const std::string& UserDisconnected::getName() const
  {
    return this->name;
  }
};
