//==================================================================================================
//    ReadTask.h
//==================================================================================================
#ifndef READ_TASK_H
#define READ_TASK_H
#include <boost/shared_ptr.hpp>
#include "ServerState.h"
#include "SocketThread.h"
#include "TaskPool.Task.h"

//==================================================================================================
///   The task for reading a persistent socket connection.
//==================================================================================================
class ReadTask : public TaskPool::Task
{
public:
  ReadTask(const boost::shared_ptr<SocketThread>& socketThread,
           const boost::shared_ptr<ServerState>& serverState);
  virtual ~ReadTask();
  void process();

protected:
  boost::shared_ptr<SocketThread> socketThread;
  boost::shared_ptr<ServerState> serverState;
};

#endif // READ_TASK_H

