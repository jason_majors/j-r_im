//==================================================================================================
///   Simple reusable socket server for school.
//==================================================================================================
#ifndef SOCKET_SERVER_H
#define SOCKET_SERVER_H
#include "ArgMap.h"
#include "SocketBase.h"
#include <unordered_map>

//==================================================================================================
///   Abstracts the connecting, accepting, reading, writing, and disconnecting.
//==================================================================================================
class SocketServer : public SocketBase
{
public:
  SocketServer(SocketBase::SocketType type, const ArgMap::ArgMap& args);
  virtual ~SocketServer();

  virtual void connect();

protected:
  int port;
  int connectionCount;
};
#endif // SOCKET_SERVER_H
