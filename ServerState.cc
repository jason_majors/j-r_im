//==================================================================================================
//    ServerState.cc
//==================================================================================================
#include "Exceptions.h"
#include "Logger.h"
#include "ServerState.h"

static const std::string CRLF = "\r\n";
//==================================================================================================
///   Constructor.
//==================================================================================================
ServerState::ServerState(TaskPool::Controller& taskController, const std::string& host, int port)
: taskController(taskController), host(host), port(port)
{
  this->readTaskCount = 0;
}

//==================================================================================================
///   Destructor.
//==================================================================================================
ServerState::~ServerState()
{
}

//==================================================================================================
///   Sets the parent for this server.
//    @param  server    The server to subscribe to.
//==================================================================================================
void ServerState::setParent(const ServerInfo& server)
{
  Lock::Scoped l(this->peerLock);
  this->parent = server;
}

//==================================================================================================
///   Adds a new child to this server.
//    @param  server    The new child server.
//==================================================================================================
void ServerState::addChild(const ServerInfo& server)
{
  Lock::Scoped l(this->peerLock);
  this->children.push_back(server);
}

//==================================================================================================
///   Returns the server's host name.
//==================================================================================================
const std::string& ServerState::getHost()
{
  return this->host;
}

//==================================================================================================
///   Returns the server's port.
//==================================================================================================
int ServerState::getPort()
{
  return this->port;
}

//==================================================================================================
///   Returns the number of active read tasks.
//==================================================================================================
size_t ServerState::getReadTaskCount()
{
  Lock::Scoped l(this->taskCountLock);
  return this->readTaskCount;
}

//==================================================================================================
///   Increases the number of active read tasks.
//==================================================================================================
size_t ServerState::addReadTask()
{
  Lock::Scoped l(this->taskCountLock);
  return ++(this->readTaskCount);
}

//==================================================================================================
///   Reduces the number of active read tasks.
//==================================================================================================
size_t ServerState::removeReadTask()
{
  Lock::Scoped l(this->taskCountLock);
  return --(this->readTaskCount);
}

//==================================================================================================
///   Adds a task to the task controller.
//==================================================================================================
void ServerState::enqueueTask(boost::shared_ptr<TaskPool::Task> task)
{
  this->taskController.enqueueTask(task);
}

//==================================================================================================
///   Tries to add a user to the map. Returns true if it can/did.
//==================================================================================================
bool ServerState::addUser(const std::string& userName, const boost::shared_ptr<SocketThread>& route)
{
  Lock::Scoped l(this->routeMapLock);
  if (this->routeMap.find(userName) != this->routeMap.end())
  {
    Logger::getLogger()->warn("ServerState: User '" + userName + "' already exists.");
    return false;
  }

  this->routeMap[userName] = route;
  Logger::getLogger()->info("ServerState: User '" + userName + "' added.");
  return true;
}

//==================================================================================================
///   Adds a client to the client list. We have to separate this from addUser() because this lists
//    local clients, while the routeMap covers every user, regardles of server.
//==================================================================================================
void ServerState::addClient(const boost::shared_ptr<SocketThread>& socket)
{
  this->clients.push_back(socket);
}

//==================================================================================================
///   Removes any instance of a client from the client list.
//==================================================================================================
void ServerState::removeClient(const boost::shared_ptr<SocketThread>& socket)
{
  for (std::list<boost::shared_ptr<SocketThread>/**/>::iterator i = this->clients.begin();
       i != this->clients.end(); i++)
  {
    if ((*i)->socketId() == socket->socketId())
    {
      this->clients.erase(i);
    }
  }
}

//==================================================================================================
///   Returns the user id connected with a socket, or an empty string.
//==================================================================================================
std::string ServerState::getUserOnSocket(const boost::shared_ptr<SocketThread>& socketThread)
{
  Lock::Scoped l(this->routeMapLock);
  for (std::unordered_map<std::string, boost::shared_ptr<SocketThread>/**/>::const_iterator i =
       this->routeMap.begin(); i != this->routeMap.end(); i++)
  {
    if (i->second == socketThread)
    {
      return i->first;
    }
  }

  return std::string();
}

//==================================================================================================
///   Returns a vector of all known users.
//==================================================================================================
boost::shared_ptr<std::vector<std::string>/**/> ServerState::getUserNames()
{
  Lock::Scoped l(this->routeMapLock);
  boost::shared_ptr<std::vector<std::string>/**/> userNames =
    boost::shared_ptr<std::vector<std::string>/**/>(new std::vector<std::string>);

  for (std::unordered_map<std::string, boost::shared_ptr<SocketThread>/**/>::const_iterator i =
       this->routeMap.begin(); i != this->routeMap.end(); i++)
  {
    userNames->push_back(i->first);
  }

  return userNames;
}

//==================================================================================================
///   Removes a user from the map.
//==================================================================================================
void ServerState::removeUser(const std::string& userName)
{
  Lock::Scoped l(this->routeMapLock);
  this->routeMap.erase(userName);
}

//==================================================================================================
///   Returns the socket that leads to a user.
//    @param  userName    The user to route.
//    @return The socketThread that leads to the user, or a null one.
//==================================================================================================
boost::shared_ptr<SocketThread> ServerState::routeToUser(const std::string& userName)
{
  Lock::Scoped l(this->routeMapLock);
  if (this->routeMap.find(userName) != this->routeMap.end())
  {
    return this->routeMap[userName];
  }

  return boost::shared_ptr<SocketThread>((SocketThread*)0);
}

//==================================================================================================
///   Writes a directive to clients of this server, unless they match the source name.
//==================================================================================================
void ServerState::writeDirectiveToOtherClients(const boost::shared_ptr<Directive::Base>& directive,
                                               const std::string& sourceUser)
{
  try
  {
    Logger::getLogger()->info("Delivering Directive to clients: " + directive->toJson());
    boost::shared_ptr<SocketThread> originSocket;
    Lock::Scoped l(this->routeMapLock);
    if (this->routeMap.find(sourceUser) != this->routeMap.end())
    {
      originSocket = this->routeMap.at(sourceUser);
    }

    std::string directiveJson(directive->toJson() + CRLF);
    for (std::list<boost::shared_ptr<SocketThread>/**/>::iterator i = this->clients.begin();
        i != this->clients.end(); i++)
    {
      Logger::getLogger()->info("Client-check.");
      if (!originSocket || originSocket->socketId() != (*i)->socketId())
      {
        Logger::getLogger()->info("Writing to the client...");
        (*i)->write(directiveJson);
      }
    }
  }
  catch(Exceptions::WriteException& e)
  {
    Logger::getLogger()->error("ServerState: Caught WriteException:");
    Logger::getLogger()->error(e.what());
  }
}

//==================================================================================================
///   Writes a directive to the peers of this server.
//==================================================================================================
void ServerState::writeDirectiveToOtherServers(const boost::shared_ptr<Directive::Base>& directive,
                                               const boost::shared_ptr<SocketThread>& source)
{
  try
  {
    Logger::getLogger()->info("Forwarding Directive to peers: " + directive->toJson());
    std::string directiveJson(directive->toJson() + CRLF);

    Lock::Scoped l(this->peerLock);
    if (this->parent.first && this->parent.first->socketId() != source->socketId())
    {
      this->parent.first->write(directiveJson);
    }

    for (std::list<ServerInfo>::iterator i = this->children.begin(); i != this->children.end(); i++)
    {
      if ((*i).first && (*i).first->socketId() != source->socketId())
      {
        (*i).first->write(directiveJson);
      }
    }
  }
  catch(Exceptions::WriteException& e)
  {
    Logger::getLogger()->error("ServerState: Caught WriteException:");
    Logger::getLogger()->error(e.what());
  }
}
