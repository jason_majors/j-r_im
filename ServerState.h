//==================================================================================================
//    ServerState.h
//==================================================================================================
#ifndef SERVER_STATE_H
#define SERVER_STATE_H

#include <boost/shared_ptr.hpp>
#include <list>
#include "Directive.Base.h"
#include "Lock.h"
#include "SocketThread.h"
#include "TaskPool.Controller.h"
//==================================================================================================
///   Stores info related to the server state.
//==================================================================================================
class ServerState
{
public:
  typedef std::pair<boost::shared_ptr<SocketThread>, Directive::Server> ServerInfo;

  ServerState(TaskPool::Controller& taskController, const std::string& host, int port);
  virtual ~ServerState();

  void setParent(const ServerInfo& server);
  //const ServerInfo& getParent();
  void addChild(const ServerInfo& server);

  const std::string& getHost();
  int getPort();
  size_t getReadTaskCount();
  size_t addReadTask();
  size_t removeReadTask();
  void enqueueTask(boost::shared_ptr<TaskPool::Task> task);
  bool addUser(const std::string& userName, const boost::shared_ptr<SocketThread>& route);
  void addClient(const boost::shared_ptr<SocketThread>& socket);
  void removeClient(const boost::shared_ptr<SocketThread>& socket);
  std::string getUserOnSocket(const boost::shared_ptr<SocketThread>& socketThread);
  boost::shared_ptr<std::vector<std::string>/**/> getUserNames();
  void removeUser(const std::string& userName);
  boost::shared_ptr<SocketThread> routeToUser(const std::string& userName);
  void writeDirectiveToOtherClients(const boost::shared_ptr<Directive::Base>& directive,
                                    const std::string& sourceUser);
  void writeDirectiveToOtherServers(const boost::shared_ptr<Directive::Base>& directive,
                                    const boost::shared_ptr<SocketThread>& source);

protected:
  TaskPool::Controller& taskController;
  std::string host;
  int port;
  size_t readTaskCount;
  Lock::Shared taskCountLock;
  Lock::Shared routeMapLock;
  Lock::Shared peerLock;
  std::unordered_map<std::string, boost::shared_ptr<SocketThread>/**/> routeMap;
  ServerInfo parent;
  std::list<ServerInfo> children;
  std::list<boost::shared_ptr<SocketThread>/**/> clients;
};

#endif // SERVER_STATE_H
