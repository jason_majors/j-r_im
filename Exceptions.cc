//==================================================================================================
//    Exceptions.cc
//==================================================================================================
#include "Exceptions.h"
namespace Exceptions
{
  //================================================================================================
  ///   Constructor.
  //    @param  what    The message.
  //================================================================================================
  ArchiveException::ArchiveException(const std::string& what)
  : std::runtime_error(what)
  {
  }

  //================================================================================================
  ///   Constructor.
  //    @param  what    The message.
  //================================================================================================
  DisconnectException::DisconnectException(const std::string& what)
  : std::runtime_error(what)
  {
  }

  //================================================================================================
  ///   Constructor.
  //    @param  what    The message.
  //================================================================================================
  ReadException::ReadException(const std::string& what)
  : std::runtime_error(what)
  {
  }

  //================================================================================================
  ///   Constructor.
  //    @param  what    The message.
  //================================================================================================
  WriteException::WriteException(const std::string& what)
  : std::runtime_error(what)
  {
  }

  //================================================================================================
  ///   Constructor.
  //    @param  what    The message.
  //================================================================================================
  ConnectException::ConnectException(const std::string& what)
  : std::runtime_error(what)
  {
  }
};
