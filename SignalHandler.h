//==================================================================================================
//    SignalHandler.h
//==================================================================================================
#ifndef SIGNAL_HANDLER_H
#define SIGNAL_HANDLER_H
#include <signal.h>
#include <unordered_map>
#include <utility>

//==================================================================================================
///   Handles a given signal by calling the callback and passing the payload.
//==================================================================================================
template <typename Payload> class SignalHandler
{
private:
  typedef void(*HandlerType)(Payload*);

  struct HandlerData
  {
    Payload* payload;
    HandlerType handler;
  };

  typedef std::unordered_map<int, HandlerData> SignalMap;

public:
  //================================================================================================
  ///   Constructor.
  //    @param  signal    The signal to handle.
  //    @param  payload   The data to pass to the user's handler.
  //    @param  handler   The user's signal handler.
  //================================================================================================
  SignalHandler(int signal, Payload* payload, HandlerType handler)
  {
    HandlerData h;
    h.payload = payload;
    h.handler = handler;
    SignalHandler::signalMap[signal] = h;
    ::signal(signal, &SignalHandler::handleAll);
  }

private:
  //================================================================================================
  ///   The first signal callback. Looks for the user defined handler and calls that with the
  //    payload.
  //    @param  signal    The signal to handle.
  //================================================================================================
  static void handleAll(int signal)
  {
    HandlerData h = SignalHandler::signalMap[signal];
    h.handler(h.payload);
  }

  static SignalMap signalMap;
};

template <typename Payload> typename
SignalHandler<Payload>::SignalMap SignalHandler<Payload>::signalMap;

#endif // SIGNAL_HANDLER_H
