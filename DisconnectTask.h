//==================================================================================================
//    DisconnectTask.h
//==================================================================================================
#ifndef DISCONNECT_TASK_H
#define DISCONNECT_TASK_H
#include <boost/shared_ptr.hpp>
#include "Directive.Disconnect.h"
#include "ServerState.h"
#include "SocketThread.h"
#include "TaskPool.Task.h"

//==================================================================================================
///   Handles the work required to disconnect a client.
//==================================================================================================
class DisconnectTask : public TaskPool::Task
{
public:
  DisconnectTask(const Directive::Disconnect& directive,
                 const boost::shared_ptr<SocketThread>& socketThread,
                 const boost::shared_ptr<ServerState>& serverState);
  virtual ~DisconnectTask();

  void process();

protected:
  Directive::Disconnect directive;
  boost::shared_ptr<SocketThread> socketThread;
  boost::shared_ptr<ServerState> serverState;
};

#endif // DISCONNECT_TASK_H
