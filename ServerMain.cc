//==================================================================================================
//    ServerMain.cc
//==================================================================================================
#include "ArgMap.h"
#include "SignalHandler.h"
#include "ThreadedSocketServer.h"
#include <iostream>
#include <string>
#include <stdexcept>

void cleanUp(ThreadedSocketServer* server);
void showUsage(const char* argv0);
//==================================================================================================
//    main()
//==================================================================================================
int main(int argc, char* argv[])
{
  try
  {
    ArgMap::ArgMap args;
    args[ArgMap::MaxConnectionsKey] = ArgMap::Arg(100);
    args[ArgMap::PortKey] = ArgMap::Arg(1000);
    args[ArgMap::ThreadPoolSizeKey] = ArgMap::Arg(30);
    args[ArgMap::ReadPercentKey] = ArgMap::Arg(65);
    ArgMap::parseArgs(argc, argv, args);

    ThreadedSocketServer server(SocketBase::Tcp, args);
    SignalHandler<ThreadedSocketServer> s0(SIGINT, &server, &cleanUp);
    SignalHandler<ThreadedSocketServer> s1(SIGTERM, &server, &cleanUp);
    ::signal(SIGPIPE, SIG_IGN);
    server.connect();
    server.runLoop();
    cleanUp(&server);
  }
  catch (ArgMap::ArgumentException& e)
  {
    showUsage(argv[0]);
    std::cerr << "Bad Argument: " << e.what() << std::endl;
    std::cerr << "Bye..." << std::endl;
    ::exit(-1);
  }
  catch (std::runtime_error& e)
  {
    std::cerr << "Caught exception: " << e.what() << std::endl;
    std::cerr << "This failure weighs on my soul. I cannot go on..." << std::endl;
    ::exit(-1);
  }

  return 0;
}

//==================================================================================================
///   Waits for the threads to finish processing, then exits.
//==================================================================================================
void cleanUp(ThreadedSocketServer* server)
{
  std::cerr << "Signal Trapped!" << std::endl;
  server->stop();
  std::cerr << std::endl << "Exiting..." << std::endl;
  ::exit(0);
}

//==================================================================================================
//    showUsage()
//==================================================================================================
void showUsage(const char* argv0)
{
  std::cerr << "Usage:" << std::endl
            << argv0 << " [port=<port>] [threadPoolSize=<size>] [subscribePort=<port>] "
            << "[subscribeHost=<host>] " << std::endl
            << "Runs a simple IM server on that port." << std::endl << std::endl;
}
