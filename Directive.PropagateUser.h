//==================================================================================================
//    Directive.PropagateUser.h
//==================================================================================================
#ifndef DIRECTIVE_PROPAGATE_USER_H
#define DIRECTIVE_PROPAGATE_USER_H
#include "Directive.Base.h"

namespace Directive
{
  //================================================================================================
  ///   Directive for when a client wishes to connect to a server.
  //================================================================================================
  class PropagateUser : public Directive::Base
  {
  public:
    PropagateUser();
    PropagateUser(const std::string& desiredUserName);
    virtual ~PropagateUser();
    std::string toJson() const;
    void fromJson(const std::string& data);
    Directive::Id id() const;
    const std::string& getName() const;

  protected:
    std::string desiredUserName;
    const std::string UserNameKey = "desiredUserName";
  };
};

#endif // DIRECTIVE_PROPAGATE_USER_H
