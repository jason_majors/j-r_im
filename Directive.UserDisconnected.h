//==================================================================================================
//    Directive.UserDisconnected.h
//==================================================================================================
#ifndef DIRECTIVE_USER_DISCONNECTED_H
#define DIRECTIVE_USER_DISCONNECTED_H
#include "Directive.Base.h"
namespace Directive
{
  class UserDisconnected : public Directive::Base
  {
  public:
    UserDisconnected();
    UserDisconnected(const std::string& name);
    virtual ~UserDisconnected();
    std::string toJson() const;
    void fromJson(const std::string& data);
    Directive::Id id() const;
    const std::string& getName() const;

  protected:
    std::string name;
    const std::string UserDisconnectedKey = "UserDisconnected";
  };
};

#endif // DIRECTIVE_USER_DISCONNECTED_H
