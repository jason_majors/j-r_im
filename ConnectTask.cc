//==================================================================================================
//    ConnectTask.cc
//==================================================================================================
#include "Exceptions.h"
#include "Logger.h"
#include "ConnectTask.h"
#include "Directive.UserConnected.h"
#include "Directive.PropagateUser.h"
#include "Exceptions.h"

static const std::string CRLF = "\r\n";
//==================================================================================================
///   Constructor.
//==================================================================================================
ConnectTask::ConnectTask(const Directive::Connect& directive,
                         const boost::shared_ptr<SocketThread>& socketThread,
                         const boost::shared_ptr<ServerState>& serverState)
: directive(directive), socketThread(socketThread), serverState(serverState)
{
}

//==================================================================================================
///   Destructor.
//==================================================================================================
ConnectTask::~ConnectTask()
{
}

//==================================================================================================
///   Called when the task starts.
//==================================================================================================
void ConnectTask::process()
{
  try
  {
    Logger::getLogger()->info("ConnectTask: " + this->directive.toJson());

    boost::uint32_t responseCode = Directive::ConnectResponse::NameTaken;
    if (this->serverState->addUser(this->directive.getName(), this->socketThread))
    {
      this->serverState->addClient(this->socketThread);
      responseCode = Directive::ConnectResponse::Success;
    }

    this->socketThread->write(
        Directive::ConnectResponse(responseCode, this->directive.getName()).toJson() + CRLF);
    if (responseCode != Directive::ConnectResponse::Success)
    {
      Logger::getLogger()->error("ConnectTask: Cannot connect user.");
      return;
    }

    this->serverState->writeDirectiveToOtherClients(
        boost::shared_ptr<Directive::Base>(new Directive::UserConnected(this->directive.getName())),
        this->directive.getName());

    this->serverState->writeDirectiveToOtherServers(
        boost::shared_ptr<Directive::Base>(new Directive::PropagateUser(this->directive.getName())),
        this->socketThread);
  }
  catch(Exceptions::WriteException& e)
  {
    Logger::getLogger()->error("ConnectTask: Caught WriteException:");
    Logger::getLogger()->error(e.what());
  }
}
