//==================================================================================================
//    Directive.UserConnected.cc
//==================================================================================================
#include "Directive.UserConnected.h"

namespace Directive
{
  //================================================================================================
  ///   Default constructor.
  //================================================================================================
  UserConnected::UserConnected()
  {
  }

  //================================================================================================
  ///   Useful constructor.
  //================================================================================================
  UserConnected::UserConnected(const std::string& name) : name(name)
  {
  }

  //================================================================================================
  ///   Destructor.
  //================================================================================================
  UserConnected::~UserConnected()
  {
  }

  //================================================================================================
  ///   Converts to json.
  //================================================================================================
  std::string UserConnected::toJson() const
  {
    json::object o;
    o.append(Directive::Base::IdKey, Directive::asStr(this->id()));
    o.append(Directive::UserConnected::UserConnectedKey, this->name);
    return json::encode(o);
  }

  //================================================================================================
  ///   Converts from json.
  //================================================================================================
  void UserConnected::fromJson(const std::string& data)
  {
    json::value j = json::parse(data);
    if (j.is_object())
    {
      if (j.has_key(Directive::UserConnected::UserConnectedKey))
      {
        json::value name = j[Directive::UserConnected::UserConnectedKey];
        if (name.is_string())
        {
          this->name = name.to_string();
        }
      }
    }
  }

  //================================================================================================
  ///   Returns the id.
  //================================================================================================
  Directive::Id UserConnected::id() const
  {
    return Directive::UserConnectedId;
  }

  //================================================================================================
  ///   Returns the user name contained in this User Connected message.
  //================================================================================================
  const std::string& UserConnected::getName() const
  {
    return this->name;
  }
};
