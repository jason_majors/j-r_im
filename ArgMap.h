//==================================================================================================
//    ArgMap.h
//==================================================================================================
#ifndef ARG_MAP_H
#define ARG_MAP_H
#include <cstdlib>
#include <stdexcept>
#include <string>
#include <unordered_map>

namespace ArgMap
{
  const std::string MaxAgeKey = "maxAge";
  const std::string MaxCacheEntriesKey = "maxCacheEntries";
  const std::string MaxConnectionsKey = "maxConnections";
  const std::string PortKey = "port";
  const std::string SaveIntervalKey = "saveInterval";
  const std::string ThreadPoolSizeKey = "threadPoolSize";
  const std::string ReadPercentKey = "readPercent";
  const std::string SubscribeHostKey = "subscribeHost";
  const std::string SubscribePortKey = "subscribePort";

  //================================================================================================
  ///   Stores an arg as a string, with easy conversion to/from ints and other types.
  //================================================================================================
  class Arg
  {
  public:
    Arg();
    Arg(long a);
    Arg(const std::string& a);
    virtual ~Arg();

    long toLong() const;
    const std::string& toString() const;

  private:
    std::string value;
  };

  typedef std::unordered_map<std::string, ArgMap::Arg> ArgMap;
  void parseArgs(int argc, char* argv[], ArgMap& args);
  //================================================================================================
  ///   Specialized exception for argument errors.
  //================================================================================================
  class ArgumentException : public std::runtime_error
  {
  public:
    explicit ArgumentException(const std::string& what);
  };
};

#endif // ARG_MAP_H
