//==================================================================================================
//    Directive.ListResponse.h
//==================================================================================================
#ifndef DIRECTIVE_LIST_RESPONSE_H
#define DIRECTIVE_LIST_RESPONSE_H
#include <vector>
#include "Directive.Base.h"

namespace Directive
{
  //================================================================================================
  ///   Directive for when a client wishes to connect to a server.
  //================================================================================================
  class ListResponse : public Directive::Base
  {
  public:
    ListResponse();
    ListResponse(const boost::shared_ptr<std::vector<std::string>/**/>& users);
    virtual ~ListResponse();
    std::string toJson() const;
    void fromJson(const std::string& data);
    Directive::Id id() const;
    const std::vector<std::string>& getUsers() const;

  protected:
    boost::shared_ptr<std::vector<std::string>/**/> users;
    const std::string UserListKey = "UserInfoList";
  };
};

#endif // DIRECTIVE_LIST_RESPONSE_H
