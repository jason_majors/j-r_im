//==================================================================================================
//    ListRequestTask.h
//==================================================================================================
#ifndef LIST_REQUEST_TASK_H
#define LIST_REQUEST_TASK_H
#include <boost/shared_ptr.hpp>
#include "Directive.ListRequest.h"
#include "ServerState.h"
#include "SocketThread.h"
#include "TaskPool.Task.h"

//==================================================================================================
///   Handles the work required to connect (and reply to) a client.
//==================================================================================================
class ListRequestTask : public TaskPool::Task
{
public:
  ListRequestTask(const Directive::ListRequest& directive,
              const boost::shared_ptr<SocketThread>& socketThread,
              const boost::shared_ptr<ServerState>& serverState);
  virtual ~ListRequestTask();

  void process();

protected:
  Directive::ListRequest directive;
  boost::shared_ptr<SocketThread> socketThread;
  boost::shared_ptr<ServerState> serverState;
};

#endif // LIST_REQUEST_TASK_H
