//==================================================================================================
//    ThreadedIMClient.h
//==================================================================================================
#ifndef THREADED_IM_CLIENT_H
#define THREADED_IM_CLIENT_H
#include "SocketClient.h"
#include "Lock.h"
#include "Exceptions.h"

//==================================================================================================
///    Implements IM Client.  Allows spawned thread for monitoring server communication.
//==================================================================================================
class ThreadedIMClient
{
public:
  enum command_type {
    CMD_NONE = 0,
    CMD_CONNECT,
    CMD_DISCONNECT,
    CMD_SHOW,
    CMD_HELP,
    CMD_QUIT,
    CMD_DIRECT_IM,
    CMD_LAST_SENT_IM,
    CMD_LAST_RECEIVED_IM,
  };

  ThreadedIMClient();
  virtual ~ThreadedIMClient();

  static void* monitorServerDirectives(void* threadArgs);
  void spawnMonitoringThread();
  void cleanUpMonitoringThread();

  void updateMyUserName(const std::string& newUserName);
  const std::string& getMyUserName() const; 

  void processConsoleInput(const std::string& inputLine);
  void runConsoleLoop();

protected:
  //================================================================================================
  ///   Shares common functionality between two threads used in this class
  //================================================================================================
  class SharedInterfaces
  {
  public:
    SharedInterfaces();
    virtual ~SharedInterfaces();

    void connectToServer(const std::string& hostname, int port);
    void disconnectFromServer();
    bool isConnectedToServer();
    
    bool checkKeepAlive();
    void disableKeepAlive();
    void enableKeepAlive();

    void updateLastReceivedUser(const std::string& userName);
    const std::string& checkLastReceivedUser();

    void clearLastSentUsers();
    void addToLastSentUsers(const std::string& userName);
    void removeFromLastSentUsers(const std::string& userName);
    const std::vector<std::string>& getLastSentUsers();

    int checkConsoleInput(int timeoutSec);
    int checkNetworkInput(int timeoutSec);

    void writeToServer(const std::string& data);
    boost::shared_ptr<std::string> readFromServer();

    void printToConsole(const std::string& line);

  private:
    Lock::Shared keepAliveLock;
    Lock::Shared lastReceivedUserLock;
    Lock::Shared lastSentUsersLock;
    Lock::Shared consoleIoLock;
    Lock::Shared networkIoLock;
    boost::shared_ptr<SocketClient> serverSocket;
    bool keepAlive;
    boost::shared_ptr<std::string> lastReceivedUser;
    boost::shared_ptr<std::vector<std::string>/**/> lastSentUsers;

    friend class ThreadedIMClient;
  };

  command_type returnCommandType(const std::string& commandName);
  void processConnectCommand(const std::string& argument);
  void processDisconnectCommand();
  void processShowCommand();
  void processHelpCommand();
  void processQuitCommand();
  void processSendIMCommand(const std::string& command, const std::string& message);
  
  pthread_t networkThreadId;
  boost::shared_ptr<SharedInterfaces> sharedInterfaces;
  bool getConsoleInput;
  boost::shared_ptr<std::string> myUserName;
  
  friend class SharedInterfaces;
};

#endif // THREADED_IM_CLIENT_H
