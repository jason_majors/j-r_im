//==================================================================================================
//    SocketBase.cc
//==================================================================================================
#include <algorithm>
#include <cerrno>
#include <cstring>
#include <stdexcept>
#include <sys/types.h>
#include <unistd.h>
#include "Exceptions.h"
#include "SocketBase.h"

const std::string CRLF = "\r\n";
const std::string CRLFCRLF = "\r\n\r\n";
//==================================================================================================
///   Constructor.
//    @param  type  The type of connection to create.
//==================================================================================================
SocketBase::SocketBase(SocketBase::SocketType type, bool closeOnDestruct)
: type(type), socket(-1), closeOnDestruct(closeOnDestruct)
{
}

//==================================================================================================
///   Destructor.
//==================================================================================================
SocketBase::~SocketBase()
{
  if (this->closeOnDestruct)
  {
    this->disconnect(false);
  }
}

//==================================================================================================
///   Closes the socket connection.
//    @param  strict    If true, throw if the socket isn't open.
//    @throws std::runtime_error If the socket is not active or the close fails.
//==================================================================================================
void SocketBase::disconnect(bool strict)
{
  if (this->isConnected())
  {
    int rc = ::close(this->socket);
    this->socket = -1;
    if (rc != 0)
    {
      Exceptions::raise<Exceptions::DisconnectException>(__FILE__, __LINE__, ::strerror(errno));
    }
  }
  else if (strict)
  {
    Exceptions::raise<Exceptions::DisconnectException>(__FILE__, __LINE__, ::strerror(errno));
  }
}

//==================================================================================================
///   Indicates whether the socket connection is active.
//    @return true/false
//==================================================================================================
bool SocketBase::isConnected() const
{
  return this->socket > 2;
}

//==================================================================================================
///   Gets the socket ID.
//    @return the socket ID
//==================================================================================================
int SocketBase::socketId() const
{
  return this->socket;
}

//==================================================================================================
///   Reads data from the socket and returns it.
//    @return A pointer to a string of the bytes.
//    @throws std::runtime_error If the socket is not active or the read fails.
//==================================================================================================
boost::shared_ptr<std::string> SocketBase::read(bool toEof) const
{
  if (!this->isConnected())
  {
    Exceptions::raise<Exceptions::ReadException>(__FILE__, __LINE__, ::strerror(errno));
  }

  boost::shared_ptr<std::string> bytes = boost::shared_ptr<std::string>(new std::string());
  while (toEof || bytes->rfind(CRLF) == std::string::npos)
  {
    char buffer[BufferSize];
    int rc = ::read(this->socket, buffer, BufferSize);

    if (rc == 0)
    {
      break;
    }
    else if (rc < 0)
    {
      Exceptions::raise<Exceptions::ReadException>(__FILE__, __LINE__, ::strerror(errno));
    }
    else
    {
      bytes->append(buffer, buffer + rc);
    }
  }

  return bytes;
}

//==================================================================================================
///   Writes data to the socket.
//    @param  data  The data (*with* the \r\n on the end).
//    @throws std::runtime_error If the socket is not active or the write fails.
//==================================================================================================
void SocketBase::write(const std::string& data) const
{
  if (!this->isConnected())
  {
    Exceptions::raise<Exceptions::WriteException>(__FILE__, __LINE__, ::strerror(errno));
  }

  int bytesWritten = 0;
  while (bytesWritten < data.length())
  {
    char buffer[BufferSize];
    int byteCount = std::min(static_cast<int>(BufferSize), int(data.length() - bytesWritten));
    for (int i = 0; i < byteCount; i++)
    {
      buffer[i] = data[i + bytesWritten];
    }

    int rc = ::send(this->socket, buffer, byteCount, MSG_NOSIGNAL);
    bytesWritten += rc;
    if (rc < 0)
    {
      Exceptions::raise<Exceptions::WriteException>(__FILE__, __LINE__, ::strerror(errno));
    }
  }
}

//==================================================================================================
/// Returns status of given socket if there is data to read before timeout expires
//  @param  socketId  The id of the socket to check for input
//  @param  timeoutSec  The number of seconds to wait for input before timing out
//  @return zero if socket is not ready to be read, non-zero otherwise
//  @throws std::runtime_error If the select call fails. 
//==================================================================================================
int SocketBase::selectReadOnly(int socketId, int timeoutSec) {
  fd_set fds;
  FD_ZERO(&fds);
  FD_SET(socketId, &fds);

  struct timeval timeout;
  timeout.tv_sec = timeoutSec;
  timeout.tv_usec = 0;

  int selectRetVal = select(socketId+1, &fds, NULL, NULL, &timeout);
  if (selectRetVal < 0) {
    Exceptions::raise<Exceptions::ReadException>(__FILE__, __LINE__, ::strerror(errno));
  }
  return FD_ISSET(socketId, &fds);
}
