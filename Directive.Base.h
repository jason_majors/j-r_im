//==================================================================================================
//    Directive.Base.h
//==================================================================================================
#ifndef DIRECTIVE_BASE_H
#define DIRECTIVE_BASE_H

#include "json.h"

namespace Directive
{
  struct Server
  {
    Server() { ; }
    Server(const std::string host, boost::uint32_t port);
    Server(const Server& rhs);
    json::object toJson() const;
    void fromJson(const json::object& jsonData);
    const Server& operator=(const Server& rhs);

    std::string host;
    boost::uint32_t port;
    const std::string HostKey = "serverHost";
    const std::string PortKey = "serverPort";
  };

  struct ImData
  {
    ImData() { ; }
    ImData(const ImData& rhs);
    json::object toJson() const;
    void fromJson(const json::object& jsonData);
    const ImData& operator=(const ImData& rhs);

    std::string origin;
    std::string target;
    std::string content;
    const std::string OriginKey = "originUser";
    const std::string TargetKey = "targetUser";
    const std::string ContentKey = "imContent";
  };

  typedef boost::uint32_t Id;
  const Id NoneId = 0;
  const Id ConnectId = 1000;
  const Id ConnectResponseId = 1001;
  const Id UserConnectedId = 1002;
  const Id DisconnectId = 1003;
  const Id UserDisconnectedId = 1004;
  const Id SendImId = 1005;
  const Id ImFailedId = 1006;
  const Id ListRequestId = 1007;
  const Id ListResponseId = 1008;

  // V2.0
  const Id SubscribeId = 1100;
  const Id SubscribeResponseId = 1101;
  const Id PropagateUserId = 1102;
  const Id RejectUserId = 1103;

  boost::uint32_t asInt(const std::string& s);
  std::string asStr(boost::uint32_t i);
  //================================================================================================
  ///   Base class for all directives to inherit from.
  //================================================================================================
  class Base
  {
  public:
    Base();
    virtual ~Base();
    virtual std::string toJson() const = 0;
    virtual void fromJson(const std::string& json) = 0;
    virtual Directive::Id id() const = 0;
    static Id parseId(const std::string& s);

  protected:
    const static std::string IdKey;
  };
};

#endif // DIRECTIVE_BASE_H
