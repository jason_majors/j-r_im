//==================================================================================================
//    Logger.h
//==================================================================================================
#ifndef LOGGER_H
#define LOGGER_H
#include <boost/shared_ptr.hpp>
#include <fstream>
#include "Lock.h"

//==================================================================================================
///   Instantiates a singleton logger.
//==================================================================================================
class Logger
{
public:
  typedef unsigned short Level;
  const static Logger::Level Info = 0;
  const static Logger::Level Warn = 1;
  const static Logger::Level Error = 2;

  static boost::shared_ptr<Logger> getLogger();
  bool log(Logger::Level messageLevel, const std::string& logMessage);
  bool info(const std::string& logMessage);
  bool warn(const std::string& logMessage);
  bool error(const std::string& logMessage);
  void logToStderr(bool doit=true);
  void logToFile(const std::string& fileName);
  void setLogLevel(Logger::Level level);
  std::string logFileName();
  static std::string stringify(Logger::Level level);

private:
  Logger();
  Logger(const Logger& l);

  Logger::Level logLevel;
  boost::shared_ptr<std::ofstream> outFileStream;
  std::string fileName;
  bool useStderr;
  static boost::shared_ptr<Logger> logger;
  static Lock::Shared lock;
};

#endif // LOGGER_H
