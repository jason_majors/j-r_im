//==================================================================================================
//    UserDisconnectedTask.h
//==================================================================================================
#ifndef USER_DISCONNECTED_TASK_H
#define USER_DISCONNECTED_TASK_H
#include <boost/shared_ptr.hpp>
#include "Directive.UserDisconnected.h"
#include "ServerState.h"
#include "SocketThread.h"
#include "TaskPool.Task.h"

//==================================================================================================
///   Handles the work required to disconnect a client.
//==================================================================================================
class UserDisconnectedTask : public TaskPool::Task
{
public:
  UserDisconnectedTask(const Directive::UserDisconnected& directive,
                 const boost::shared_ptr<SocketThread>& socketThread,
                 const boost::shared_ptr<ServerState>& serverState);
  virtual ~UserDisconnectedTask();

  void process();

protected:
  Directive::UserDisconnected directive;
  boost::shared_ptr<SocketThread> socketThread;
  boost::shared_ptr<ServerState> serverState;
};

#endif // USER_DISCONNECTED_TASK_H
