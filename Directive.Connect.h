//==================================================================================================
//    Directive.Connect.h
//==================================================================================================
#ifndef DIRECTIVE_CONNECT_H
#define DIRECTIVE_CONNECT_H
#include "Directive.Base.h"

namespace Directive
{
  //================================================================================================
  ///   Directive for when a client wishes to connect to a server.
  //================================================================================================
  class Connect : public Directive::Base
  {
  public:
    Connect();
    Connect(const std::string& desiredUserName);
    virtual ~Connect();
    std::string toJson() const;
    void fromJson(const std::string& data);
    Directive::Id id() const;
    const std::string& getName() const;

  protected:
    std::string desiredUserName;
    const std::string UserNameKey = "desiredUserName";
  };
};

#endif // DIRECTIVE_CONNECT_H
