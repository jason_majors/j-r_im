//==================================================================================================
//    SocketClient.h
//==================================================================================================
#ifndef SOCKET_CLIENT_H
#define SOCKET_CLIENT_H
#include "SocketBase.h"
#include <string>

//==================================================================================================
///   Implements a generic socket client.
//==================================================================================================
class SocketClient : public SocketBase
{
public:
  SocketClient(const std::string& hostname, int port, SocketBase::SocketType type,
               bool closeOnDestruct=true);
  virtual ~SocketClient();

  void connect();
  void reconnect();

protected:
  int port;
  std::string hostname;
};

#endif // SOCKET_CLIENT_H
