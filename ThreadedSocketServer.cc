//==================================================================================================
//    ThreadedSocketServer.cc
//==================================================================================================
#include <cerrno>
#include <cstring>
#include <iostream>
#include <netinet/in.h>
#include <sstream>
#include <stdexcept>
#include <sys/socket.h>
#include "Exceptions.h"
#include "Logger.h"
#include "ReadTask.h"
#include "SocketClient.h"
#include "SocketThread.h"
#include "SubscribeTask.h"
#include "ThreadedSocketServer.h"

static const std::string CRLF = "\r\n";
//==================================================================================================
///   Constructor.
//    @param  type        The type of socket to open.
//    @param  args        The command line arguments.
//==================================================================================================
ThreadedSocketServer::ThreadedSocketServer(SocketBase::SocketType type, const ArgMap::ArgMap& args)
: SocketServer(type, args), taskController(args), args(args)
{
  this->serverState = boost::shared_ptr<ServerState>(new ServerState(this->taskController, "",
                                                                      this->port));
  if (this->args.find(ArgMap::SubscribeHostKey) != this->args.end() &&
      this->args.find(ArgMap::SubscribePortKey) != this->args.end())
  {
    this->subscribe(this->args.at(ArgMap::SubscribeHostKey).toString(),
                    this->args.at(ArgMap::SubscribePortKey).toLong());
  }
}

//==================================================================================================
///   Destructor.
//==================================================================================================
ThreadedSocketServer::~ThreadedSocketServer()
{
}

//==================================================================================================
///   Waits for a connection attempt, and adds a new task to the pool to handle it.
//    @throws std::runtime_error If the accept call fails.
//==================================================================================================
void ThreadedSocketServer::runLoop()
{
  if (!this->isConnected())
  {
    Exceptions::raise<std::runtime_error>(__FILE__, __LINE__, "Socket is not connected.");
  }

  while (true)
  {
    struct sockaddr_in socketAddress;
    socklen_t socketLength = sizeof(sockaddr_in);
    int socketId = ::accept(this->socket, (struct sockaddr*)&socketAddress, &socketLength);
    if (socketId < 0)
    {
      Exceptions::raise<std::runtime_error>(__FILE__, __LINE__, ::strerror(errno));
    }


    if (this->serverState->getReadTaskCount() >
        (this->taskController.workerCount() * this->args.at(ArgMap::ReadPercentKey).toLong() / 100))
    {
      // TODO Write a decline directive or something.
      ::close(socketId);
    }
    else
    {
      boost::shared_ptr<SocketThread> t(new SocketThread(this->type, socketId));
      this->taskController.enqueueTask(
          boost::shared_ptr<TaskPool::Task>(new ReadTask(t, this->serverState)));
    }
  }
}

//==================================================================================================
///   Stops the threads for a clean exit.
//==================================================================================================
void ThreadedSocketServer::stop()
{
  this->taskController.stop();
}

//==================================================================================================
///   Subscribes this server to another one.
//==================================================================================================
void ThreadedSocketServer::subscribe(const std::string& host, long port)
{
  if (port < 1024 || port >= 1 << 16)
  {
    std::stringstream s;
    s << "ThreadedSocketServer: Subscription port '" << port << "' is invalid." << std::ends;
    Logger::getLogger()->error(s.str());
    return;
  }

  std::stringstream s;
  s << "ThreadedSocketServer: Subscribing to host: " << host << " on port: " << port << ".";
  Logger::getLogger()->info(s.str());

  try
  {
    SocketClient c(host, static_cast<int>(port), SocketBase::Tcp, false);
    c.connect();
    ServerState::ServerInfo s;
    s.first =
      boost::shared_ptr<SocketThread>(new SocketThread(SocketBase::Tcp, c.socketId()));
    s.second.host = host;
    s.second.port = port;
    Directive::Subscribe subscribe(s.second);
    s.first->write(subscribe.toJson() + CRLF);
    boost::shared_ptr<std::string> response = s.first->read(false);
    Directive::Id id = Directive::Base::parseId(*response);
    if (id == Directive::SubscribeResponseId)
    {
      Directive::SubscribeResponse d;
      d.fromJson(*response);
      if (d.getCode() == Directive::SubscribeResponse::Success)
      {
        for (std::vector<std::string>::const_iterator i = d.getUsers().begin();
             i != d.getUsers().end(); i++)
        {
          this->serverState->addUser(*i, s.first);
        }

        this->serverState->setParent(s);
        Logger::getLogger()->info("ThreadedSocketServer: Successfully subscribed to a server.");
        this->taskController.enqueueTask(
            boost::shared_ptr<TaskPool::Task>(new ReadTask(s.first, this->serverState)));
      }
      else
      {
        std::stringstream error;
        error << "ThreadedSocketServer: Response code from subscribe request: " << d.getCode()
              << "." << std::ends;
        Logger::getLogger()->error(error.str());
      }
    }
    else
    {
      std::stringstream error;
      error << "ThreadedSocketServer: Expecting directive '" << Directive::SubscribeResponseId
            << "', but received '" << id << "'." << std::ends;
      Logger::getLogger()->error(error.str());
    }
  }
  catch (Exceptions::ReadException& e)
  {
    Logger::getLogger()->error(e.what());
    return;
  }
  catch (Exceptions::WriteException& e)
  {
    Logger::getLogger()->error(e.what());
    return;
  }
  catch (Exceptions::ConnectException& e)
  {
    Logger::getLogger()->error(e.what());
    return;
  }
}
