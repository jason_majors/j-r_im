//==================================================================================================
//    SendImTask.h
//==================================================================================================
#ifndef SEND_IM_TASK_H
#define SEND_IM_TASK_H
#include <boost/shared_ptr.hpp>
#include "Directive.SendIm.h"
#include "Directive.ImFailed.h"
#include "ServerState.h"
#include "SocketThread.h"
#include "TaskPool.Task.h"

//==================================================================================================
///   Handles the work required to connect (and reply to) a client.
//==================================================================================================
class SendImTask : public TaskPool::Task
{
public:
  SendImTask(const Directive::SendIm& directive,
             const boost::shared_ptr<ServerState>& serverState);
  virtual ~SendImTask();

  void process();

protected:
  Directive::SendIm directive;
  boost::shared_ptr<ServerState> serverState;
};

#endif // SEND_IM_TASK_H
