//==================================================================================================
//    Directive.ImFailed.h
//==================================================================================================
#ifndef DIRECTIVE_IM_FAILED_H
#define DIRECTIVE_IM_FAILED_H
#include "Directive.Base.h"

namespace Directive
{
  //================================================================================================
  ///   Directive for when a client wishes to connect to a server.
  //================================================================================================
  class ImFailed : public Directive::Base
  {
  public:
    ImFailed();
    ImFailed(const std::string& target, const std::string& origin);
    virtual ~ImFailed();
    std::string toJson() const;
    void fromJson(const std::string& data);
    Directive::Id id() const;
    const std::string& getTarget() const;
    const std::string& getOrigin() const;

  protected:
    std::string origin;
    std::string target;
    const std::string OriginKey = "originUser";
    const std::string TargetKey = "targetUser";
  };
};

#endif // DIRECTIVE_IM_FAILED_H
