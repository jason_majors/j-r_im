//==================================================================================================
//    SendImTask.cc
//==================================================================================================
#include "Exceptions.h"
#include "Logger.h"
#include "SendImTask.h"

static const std::string CRLF = "\r\n";
//==================================================================================================
///   Constructor.
//==================================================================================================
SendImTask::SendImTask(const Directive::SendIm& directive,
                       const boost::shared_ptr<ServerState>& serverState)
: directive(directive), serverState(serverState)
{
}

//==================================================================================================
///   Destructor.
//==================================================================================================
SendImTask::~SendImTask()
{
}

//==================================================================================================
///   Called when the task starts.
//==================================================================================================
void SendImTask::process()
{
  try
  {
    Logger::getLogger()->info("SendImTask: " + this->directive.toJson());

    boost::shared_ptr<SocketThread> socket =
      this->serverState->routeToUser(this->directive.getTarget());

    if (socket)
    {
      Logger::getLogger()->info("SendImTask: forwarding IM to " +
                                this->directive.getTarget() + ".");
      socket->write(this->directive.toJson() + CRLF);
    }
    else
    {
      Logger::getLogger()->error("SendImTask: No route to " + this->directive.getTarget() + ".");
      boost::shared_ptr<SocketThread> socket =
        this->serverState->routeToUser(this->directive.getOrigin());

      if (socket)
      {
        Directive::ImFailed failed(this->directive.getTarget(), this->directive.getOrigin());
        Logger::getLogger()->info("SendImTask: IM Failed to " + this->directive.getOrigin() + ".");
        socket->write(failed.toJson() + CRLF);
      }
    }
  }
  catch(Exceptions::WriteException& e)
  {
    Logger::getLogger()->error("SendImTask: Caught WriteException:");
    Logger::getLogger()->error(e.what());
  }
}
