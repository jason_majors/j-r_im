//==================================================================================================
//    Directive.ListResponse.cc
//==================================================================================================
#include "Directive.ListResponse.h"

namespace Directive
{
  //================================================================================================
  ///   Default constructor.
  //================================================================================================
  ListResponse::ListResponse()
  {
    //init to empty otherwise shared_ptr will be false in fromJson call
    this->users = boost::shared_ptr<std::vector<std::string>/**/>(new std::vector<std::string>);
  }

  //================================================================================================
  ///   Constructor with a parameter.
  //    @param users    The list of users.
  //================================================================================================
  ListResponse::ListResponse(const boost::shared_ptr<std::vector<std::string>/**/>& users)
  : users(users)
  {
  }

  //================================================================================================
  ///   Destructor.
  //================================================================================================
  ListResponse::~ListResponse()
  {
  }

  //================================================================================================
  ///   Converts the directive to json format.
  //    @return   The json formatted string.
  //================================================================================================
  std::string ListResponse::toJson() const
  {
    json::object o;
    o.append(Directive::Base::IdKey, Directive::asStr(this->id()));
    json::array a;
    for (std::vector<std::string>::const_iterator i = users->begin(); i != users->end(); i++)
    {
      a.append(*i);
    }

    o.append(Directive::ListResponse::UserListKey, a);
    return json::encode(o);
  }

  //================================================================================================
  ///   Converts the directive from json.
  //    @param    json    The json formatted data.
  //================================================================================================
  void ListResponse::fromJson(const std::string& data)
  {
    json::value j = json::parse(data);
    if (j.is_object())
    {
      if (!j.has_key(Directive::ListResponse::UserListKey))
      {
        return;
      }

      json::value users = j[Directive::ListResponse::UserListKey];
      if (users.is_array())
      {
        json::array a(users.to_array());
        for (int i = 0; i < a.size(); i++)
        {
          if (a[i].is_string())
          {
            this->users->push_back(a[i].to_string());
          }
        }
      }
    }
  }

  //================================================================================================
  ///   Returns the Directive ID.
  //================================================================================================
  Directive::Id ListResponse::id() const
  {
    return Directive::ListResponseId;
  }

  //================================================================================================
  ///   Returns the Directive ID.
  //================================================================================================
  const std::vector<std::string>& ListResponse::getUsers() const
  {
    return *(this->users);
  }
};
