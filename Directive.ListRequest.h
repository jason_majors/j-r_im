//==================================================================================================
//    Directive.ListRequest.h
//==================================================================================================
#ifndef DIRECTIVE_LIST_REQUEST_H
#define DIRECTIVE_LIST_REQUEST_H
#include "Directive.Base.h"

namespace Directive
{
  //================================================================================================
  ///   Directive for when a client disconnects from a server.
  //================================================================================================
  class ListRequest : public Directive::Base
  {
  public:
    ListRequest();
    virtual ~ListRequest();
    std::string toJson() const;
    void fromJson(const std::string& data);
    Directive::Id id() const;
  };
};

#endif // DIRECTIVE_LIST_REQUEST_H
