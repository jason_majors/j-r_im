//==================================================================================================
//    Directive.SendIm.h
//==================================================================================================
#ifndef DIRECTIVE_SEND_IM_H
#define DIRECTIVE_SEND_IM_H
#include "Directive.Base.h"
namespace Directive
{
  class SendIm : public Directive::Base
  {
  public:
    SendIm();
    SendIm(const Directive::ImData& im);
    virtual ~SendIm();
    std::string toJson() const;
    void fromJson(const std::string& data);
    Directive::Id id() const;
    std::string getOrigin() const;
    std::string getTarget() const;
    std::string getContent() const;

  protected:
    Directive::ImData imData;
    const std::string ImDataStructKey = "imDataStruct";
  };
};

#endif // DIRECTIVE_SEND_IM_H
