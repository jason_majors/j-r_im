//==================================================================================================
//    PropagateUserTask.cc
//==================================================================================================
#include "Exceptions.h"
#include "Logger.h"
#include "PropagateUserTask.h"
#include "Directive.UserConnected.h"
#include "Directive.ConnectResponse.h"

//==================================================================================================
///   Constructor.
//==================================================================================================
PropagateUserTask::PropagateUserTask(const Directive::PropagateUser& directive,
                                     const boost::shared_ptr<SocketThread>& socketThread,
                                     const boost::shared_ptr<ServerState>& serverState)
: directive(directive), socketThread(socketThread), serverState(serverState)
{
}

//==================================================================================================
///   Destructor.
//==================================================================================================
PropagateUserTask::~PropagateUserTask()
{
}

//==================================================================================================
///   Called when the task starts.
//==================================================================================================
void PropagateUserTask::process()
{
  try
  {
    Logger::getLogger()->info("PropagateUserTask: " + this->directive.toJson());
    if (this->serverState->addUser(this->directive.getName(), this->socketThread))
    {
      this->serverState->writeDirectiveToOtherServers(
        boost::shared_ptr<Directive::Base>(new Directive::PropagateUser(this->directive.getName())),
        this->socketThread);
      this->serverState->writeDirectiveToOtherClients(
        boost::shared_ptr<Directive::Base>(new Directive::UserConnected(this->directive.getName())),
        this->directive.getName());
    }
    else
    {
      Logger::getLogger()->info("PropagateUserTask: Rejecting propagation.");
      Directive::ConnectResponse r(Directive::ConnectResponse::NameTaken,
                                   this->directive.getName());
      this->socketThread->write(r.toJson());
    }
  }
  catch(Exceptions::WriteException& e)
  {
    Logger::getLogger()->error("PropagateUserTask: Caught WriteException:");
    Logger::getLogger()->error(e.what());
  }
}
