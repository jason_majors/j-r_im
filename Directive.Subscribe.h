//==================================================================================================
//    Directive.Subscribe.h
//==================================================================================================
#ifndef DIRECTIVE_SUBSCRIBE_H
#define DIRECTIVE_SUBSCRIBE_H
#include "Directive.Base.h"

namespace Directive
{
  //================================================================================================
  ///   Directive for when a client wishes to connect to a server.
  //================================================================================================
  class Subscribe : public Directive::Base
  {
  public:
    Subscribe();
    Subscribe(const Directive::Server& server);
    virtual ~Subscribe();
    std::string toJson() const;
    void fromJson(const std::string& data);
    Directive::Id id() const;
    const Directive::Server& getServer() const;

  protected:
    Directive::Server server;
    const std::string ServerInfoKey = "serverInfo";
  };
};

#endif // DIRECTIVE_SUBSCRIBE_H
