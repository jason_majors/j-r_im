//==================================================================================================
//    Directive.SubscribeResponse.h
//==================================================================================================
#ifndef DIRECTIVE_SUBSCRIBE_RESPONSE_H
#define DIRECTIVE_SUBSCRIBE_RESPONSE_H
#include <vector>
#include "Directive.Base.h"
namespace Directive
{
  class SubscribeResponse : public Directive::Base
  {
  public:
    static const boost::uint32_t Success = 2100;
    static const boost::uint32_t Error = 2101;

    SubscribeResponse();
    SubscribeResponse(boost::uint32_t code, const std::vector<std::string>& users);
    virtual ~SubscribeResponse();
    std::string toJson() const;
    void fromJson(const std::string& data);
    Directive::Id id() const;
    boost::uint32_t getCode() const;
    const std::vector<std::string>& getUsers() const;

  protected:
    boost::uint32_t code;
    std::vector<std::string> users;
    const std::string CodeKey = "code";
    const std::string UsersKey = "users";
  };
};

#endif // DIRECTIVE_SUBSCRIBE_RESPONSE_H
