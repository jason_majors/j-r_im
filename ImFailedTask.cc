//==================================================================================================
//    ImFailedTask.cc
//==================================================================================================
#include "Exceptions.h"
#include "Logger.h"
#include "ImFailedTask.h"

static const std::string CRLF = "\r\n";
//==================================================================================================
///   Constructor.
//==================================================================================================
ImFailedTask::ImFailedTask(const Directive::ImFailed& directive,
                           const boost::shared_ptr<ServerState>& serverState)
: directive(directive), serverState(serverState)
{
}

//==================================================================================================
///   Destructor.
//==================================================================================================
ImFailedTask::~ImFailedTask()
{
}

//==================================================================================================
///   Forwards the directive to the origin, if a path exists. Otherwise, just drop it.
//==================================================================================================
void ImFailedTask::process()
{
  try
  {
    Logger::getLogger()->info("ImFailedTask: " + this->directive.toJson());

    boost::shared_ptr<SocketThread> socket =
      this->serverState->routeToUser(this->directive.getOrigin());

    if (socket)
    {
      Logger::getLogger()->info("ImFailedTask: forwarding to " + this->directive.getOrigin() + ".");
      socket->write(this->directive.toJson() + CRLF);
    }
  }
  catch(Exceptions::WriteException& e)
  {
    Logger::getLogger()->error("ImFailedTask: Caught WriteException:");
    Logger::getLogger()->error(e.what());
  }
}
