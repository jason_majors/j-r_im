//==================================================================================================
//    ReadTask.cc
//==================================================================================================
#include <sstream>
#include "ConnectTask.h"
#include "DisconnectTask.h"
#include "Exceptions.h"
#include "ImFailedTask.h"
#include "ListRequestTask.h"
#include "Logger.h"
#include "PropagateUserTask.h"
#include "ReadTask.h"
#include "RejectUserTask.h"
#include "SendImTask.h"
#include "ServerDirectives.h"
#include "SubscribeTask.h"
#include "UserDisconnectedTask.h"

//==================================================================================================
///   Constructor.
//==================================================================================================
ReadTask::ReadTask(const boost::shared_ptr<SocketThread>& socketThread,
                   const boost::shared_ptr<ServerState>& serverState)
: socketThread(socketThread), serverState(serverState)
{
  this->serverState->addReadTask();
}

//==================================================================================================
///   Destructor.
//==================================================================================================
ReadTask::~ReadTask()
{
  this->serverState->removeReadTask();
  this->socketThread->disconnect();
}

//==================================================================================================
///   Listens for requests and processes them.
//==================================================================================================
void ReadTask::process()
{
  while (true)
  {
    Logger::getLogger()->info("Loop...");
    boost::shared_ptr<std::string> data = this->socketThread->read();
    std::stringstream s;
    s << "ReadTask: Received data'" << *data << "' from socket: "
      << this->socketThread->socketId() << "." << std::ends;
    Logger::getLogger()->info(s.str());

    if (data->length() == 0)
    {
      std::string myUser = this->serverState->getUserOnSocket(this->socketThread);
      Logger::getLogger()->info("ReadTask: User " + myUser + " disconnected. Removing it.");
      this->serverState->removeUser(myUser);
      return;
    }

    try
    {
      Directive::Id id = Directive::Base::parseId(*data);
      if (id == Directive::ConnectId)
      {
        Logger::getLogger()->info("Processing as Directive::Connect.");
        Directive::Connect d;
        d.fromJson(*data);
        this->serverState->enqueueTask(
            boost::shared_ptr<TaskPool::Task>(new ConnectTask(d, this->socketThread,
                                                              this->serverState)));
      }
      else if (id == Directive::DisconnectId)
      {
        Directive::Disconnect d;
        d.fromJson(*data);
        this->serverState->enqueueTask(
            boost::shared_ptr<TaskPool::Task>(new DisconnectTask(d, this->socketThread,
                                                                 this->serverState)));
      }
      else if (id == Directive::UserDisconnectedId)
      {
        Directive::UserDisconnected d;
        d.fromJson(*data);
        this->serverState->enqueueTask(
            boost::shared_ptr<TaskPool::Task>(new UserDisconnectedTask(d, this->socketThread,
                                                                       this->serverState)));
      }
      else if (id == Directive::SendImId)
      {
        Directive::SendIm d;
        d.fromJson(*data);
        this->serverState->enqueueTask(
            boost::shared_ptr<TaskPool::Task>(new SendImTask(d, this->serverState)));
      }
      else if (id == Directive::ImFailedId)
      {
        Directive::ImFailed d;
        d.fromJson(*data);
        this->serverState->enqueueTask(
            boost::shared_ptr<TaskPool::Task>(new ImFailedTask(d, this->serverState)));
      }
      else if (id == Directive::ListRequestId)
      {
        Directive::ListRequest d;
        d.fromJson(*data);
        this->serverState->enqueueTask(
            boost::shared_ptr<TaskPool::Task>(new ListRequestTask(d, this->socketThread,
                                                                  this->serverState)));
      }
      else if (id == Directive::SubscribeId)
      {
        Logger::getLogger()->info("Processing as Directive::Subscribe.");
        Directive::Subscribe d;
        d.fromJson(*data);
        this->serverState->enqueueTask(
            boost::shared_ptr<TaskPool::Task>(new SubscribeTask(d, this->socketThread,
                                                                this->serverState)));
      }
      else if (id == Directive::PropagateUserId)
      {
        Logger::getLogger()->info("Processing as Directive::PropagateUser.");
        Directive::PropagateUser d;
        d.fromJson(*data);
        this->serverState->enqueueTask(
            boost::shared_ptr<TaskPool::Task>(new PropagateUserTask(d, this->socketThread,
                                                                    this->serverState)));
      }
      else if (id == Directive::ConnectResponseId)
      {
        Logger::getLogger()->info("Processing as Directive::ConnectResponse.");
        Directive::ConnectResponse d;
        d.fromJson(*data);
        if (d.getCode() == Directive::ConnectResponse::NameTaken)
        {
          this->serverState->enqueueTask(
              boost::shared_ptr<TaskPool::Task>(new RejectUserTask(d, this->socketThread,
                                                                      this->serverState)));
        }
      }
      else
      {
        std::stringstream s1;
        s1 << "Invalid Directive ID: " << id << "." << std::ends;
        Logger::getLogger()->error(s1.str());
      }
    }
    catch(json::value_expected& e)
    {
      Logger::getLogger()->error("Invalid json '" + *data + "'.");
    }
    catch(Exceptions::ReadException& e)
    {
      Logger::getLogger()->error("ReadTask: Caught ReadException:");
      Logger::getLogger()->error(e.what());
    }
  }
}
