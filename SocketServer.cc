//==================================================================================================
//    SocketServer.cc
//==================================================================================================
#include <cerrno>
#include <cstring>
#include <netinet/in.h>
#include <stdexcept>
#include <sys/socket.h>
#include <sys/types.h>
#include "Exceptions.h"
#include "SocketServer.h"

//==================================================================================================
///   Constructor.
//    @param  type        The type of socket to open.
//    @param  port        The port to listen on.
//    @param  connectionCount The queue size for ::listen().
//==================================================================================================
SocketServer::SocketServer(SocketBase::SocketType type, const ArgMap::ArgMap& args)
: SocketBase(type)
{
  this->port = args.at(ArgMap::PortKey).toLong();
  this->connectionCount = args.at(ArgMap::MaxConnectionsKey).toLong();
}

//==================================================================================================
///   Destructor.
//==================================================================================================
SocketServer::~SocketServer()
{
}

//==================================================================================================
///   Listens for incoming connections.
//    @throws std::runtime_error If the connection fails or there is bad connection data.
//==================================================================================================
void SocketServer::connect()
{
  struct sockaddr_in socketAddress;
  ::memset(&socketAddress, 0, sizeof(sockaddr_in));
  socketAddress.sin_family = AF_INET;
  socketAddress.sin_port = htons(this->port); // ::htons is an error with -O2, gcc must optimize it.
  socketAddress.sin_addr.s_addr = INADDR_ANY;
  this->socket = ::socket(AF_INET, this->type, 0);
  if (this->socket < 0)
  {
    Exceptions::raise<Exceptions::ConnectException>(__FILE__, __LINE__, ::strerror(errno));
  }

  int rc = ::bind(this->socket, (struct sockaddr*)&socketAddress, sizeof(sockaddr_in));
  if (rc != 0)
  {
    Exceptions::raise<Exceptions::ConnectException>(__FILE__, __LINE__, ::strerror(errno));
  }

  rc = ::listen(this->socket, this->connectionCount);
  if (rc != 0)
  {
    Exceptions::raise<Exceptions::ConnectException>(__FILE__, __LINE__, ::strerror(errno));
  }
}
