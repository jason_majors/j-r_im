//==================================================================================================
//    Exceptions.h
//==================================================================================================
#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H
#include <sstream>
#include <stdexcept>
#include <string>

namespace Exceptions
{
  class ArchiveException : public std::runtime_error
  {
  public:
    ArchiveException(const std::string& what);
  };

  class DisconnectException : public std::runtime_error
  {
  public:
    DisconnectException(const std::string& what);
  };

  class ReadException : public std::runtime_error
  {
  public:
    ReadException(const std::string& what);
  };

  class WriteException : public std::runtime_error
  {
  public:
    WriteException(const std::string& what);
  };

  class ConnectException : public std::runtime_error
  {
  public:
    ConnectException(const std::string& what);
  };

  //================================================================================================
  ///   Formats and throws an exception.
  //    @param  E       An exception type.
  //    @param  file    The file where the exception occurred.
  //    @param  line    The line number where the exception occurred.
  //    @param  body    The message body of the exception.
  //    @throws An exception of type E.
  //================================================================================================
  template <typename E> void raise(const std::string& file, int line, const std::string& body)
  {
    std::stringstream s;
    s << "At " << file << ":" << line << ":: " << body << std::ends;
    throw E(s.str());
  }
};

#endif // EXCEPTIONS_H
