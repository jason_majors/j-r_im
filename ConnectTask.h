//==================================================================================================
//    ConnectTask.h
//==================================================================================================
#ifndef CONNECT_TASK_H
#define CONNECT_TASK_H
#include <boost/shared_ptr.hpp>
#include "Directive.Connect.h"
#include "Directive.ConnectResponse.h"
#include "ServerState.h"
#include "SocketThread.h"
#include "TaskPool.Task.h"

//==================================================================================================
///   Handles the work required to connect (and reply to) a client.
//==================================================================================================
class ConnectTask : public TaskPool::Task
{
public:
  ConnectTask(const Directive::Connect& directive,
              const boost::shared_ptr<SocketThread>& socketThread,
              const boost::shared_ptr<ServerState>& serverState);
  virtual ~ConnectTask();

  void process();

protected:
  Directive::Connect directive;
  boost::shared_ptr<SocketThread> socketThread;
  boost::shared_ptr<ServerState> serverState;
};

#endif // CONNECT_TASK_H
