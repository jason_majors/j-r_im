//#include "Directive.ListResponse.h"
#include "Directive.Connect.h"
#include "Directive.SendIm.h"
#include "Directive.Subscribe.h"

#include <boost/cstdint.hpp>
#include <cstdlib>
#include <iostream>
#include <string>
#include <sys/time.h>

using namespace std;

int main(int argc, char* argv[])
{
  for (int i = 1; i < argc; i++)
  {
    struct timeval before;
    struct timeval after;
    struct timeval elapsed;
    struct timezone tz;
    gettimeofday(&before, &tz);
    Directive::Connect c0(string(argv[i]));
    string connect = c0.toJson();
    Directive::Connect c1;
    c1.fromJson(connect);
    gettimeofday(&after, &tz);

    cout << "####" << c1.toJson() << "####" << endl;
    boost::uint64_t b = before.tv_sec * 1000000 + before.tv_usec;
    boost::uint64_t a = after.tv_sec * 1000000 + after.tv_usec;
    cout << "Time: " << a - b << "us." << endl;
    //cout << "ID: " << Directive::Base::parseId(connect) << endl;
    if (Directive::Base::parseId(connect) == Directive::ConnectId)
    {
      cout << "Id comparison works." << endl;
    }
  }

  Directive::Server s;
  s.host = "localhost";
  s.port = 1234;
  std::string o = "FromMe";

  std::string t("ForHim");

  Directive::ImData imData;
  imData.origin = o;
  imData.target = t;
  imData.content = "Watson, come here. I need you.";
  Directive::SendIm sendIm(imData);
  cout << "SendIm:" << endl << "####" << endl << sendIm.toJson() << endl << "####" << endl;

  Directive::Subscribe subscribe(s);
  cout << "Subscribe:" << endl << "####" << endl << subscribe.toJson() << endl << "####" << endl;
  
  return 0;
}

/*
int main(int argc, char* argv[])
{
  vector<Directive::User> users;
  for (int i = 1; i < argc; i++)
  {
    Directive::Server s;
    s.port = 12345;
    s.host = "localhost";
    Directive::User u;
    u.name = string(argv[i]);
    u.server = s;
    users.push_back(u);
  }

  
  struct timeval before;
  struct timeval after;
  struct timeval elapsed;
  struct timezone tz;
  gettimeofday(&before, &tz);
  Directive::ListResponse lr0(users);
  string listResponse = lr0.toJson();
  Directive::ListResponse lr1;
  lr1.fromJson(listResponse);
  gettimeofday(&after, &tz);

  cout << "####" << lr1.toJson() << "####" << endl;
  boost::uint64_t b = before.tv_sec * 1000000 + before.tv_usec;
  boost::uint64_t a = after.tv_sec * 1000000 + after.tv_usec;
  cout << "Time: " << a - b << "us." << endl;
  
  return 0;
}
*/
