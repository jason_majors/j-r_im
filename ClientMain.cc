//==================================================================================================
//    ClientMain.cc
//==================================================================================================
#include "ThreadedIMClient.h"
#include "SignalHandler.h"
#include <iostream>
#include <string>
#include <stdexcept>

void cleanUp(ThreadedIMClient* client);
void showUsage(const char* argv0);
//==================================================================================================
//    main()
//==================================================================================================
int main(int argc, char* argv[])
{
  try
  {
    //Register Signal Handlers
    ThreadedIMClient imClient;
    SignalHandler<ThreadedIMClient> s0(SIGINT, &imClient, &cleanUp);
    SignalHandler<ThreadedIMClient> s1(SIGTERM, &imClient, &cleanUp);
    ::signal(SIGPIPE, SIG_IGN);

    //parse args
    if (argc > 2)
    {
      showUsage(argv[0]);
      std::cerr << "Too many arguments" << std::endl;
      ::exit(-1);
    }
    else if (argc == 2)
    {
      std::string initialConnect = "/connect ";
      initialConnect.append(argv[1]);
      imClient.processConsoleInput(initialConnect);
    }

    //Run consoleloop
    imClient.runConsoleLoop();
  }
  catch (std::runtime_error& e)
  {
    std::cerr << "Caught exception: " << e.what() << std::endl;
    std::cerr << "Exiting imClient application." << std::endl;
    ::exit(-1);
  }

  return 0;
}

//==================================================================================================
///   Waits for the threads to finish processing, then exits.
//==================================================================================================
void cleanUp(ThreadedIMClient* client)
{
  std::cerr << "Signal Trapped!" << std::endl;
  client->cleanUpMonitoringThread();
  std::cerr << std::endl << "Exiting..." << std::endl;
  ::exit(0);
}

//==================================================================================================
///   showUsage()
//==================================================================================================
void showUsage(const char* argv0)
{
  std::cerr << "Usage:" << std::endl
            << argv0 << " [<user>@<host>:<port>]" << std::endl
            << "Runs a simple IM client that connectes to IM server at given hostname and port."
            << std::endl << std::endl;
}
