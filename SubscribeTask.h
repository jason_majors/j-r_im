//==================================================================================================
//    SubscribeTask.h
//==================================================================================================
#ifndef SUBSCRIBE_TASK_H
#define SUBSCRIBE_TASK_H
#include <boost/shared_ptr.hpp>
#include "Directive.Subscribe.h"
#include "Directive.SubscribeResponse.h"
#include "ServerState.h"
#include "SocketThread.h"
#include "TaskPool.Task.h"

//==================================================================================================///   Handles the work required to connect (and reply to) a subscribing service.
//==================================================================================================
class SubscribeTask : public TaskPool::Task
{
public:
  SubscribeTask(const Directive::Subscribe& directive,
              const boost::shared_ptr<SocketThread>& socketThread,
              const boost::shared_ptr<ServerState>& serverState);
  virtual ~SubscribeTask();

  void process();

protected:
  Directive::Subscribe directive;
  boost::shared_ptr<SocketThread> socketThread;
  boost::shared_ptr<ServerState> serverState;
};

#endif // SUBSCRIBE_TASK_H
