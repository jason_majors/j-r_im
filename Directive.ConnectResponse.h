//==================================================================================================
//    Directive.ConnectResponse.h
//==================================================================================================
#ifndef DIRECTIVE_CONNECT_RESPONSE_H
#define DIRECTIVE_CONNECT_RESPONSE_H
#include "Directive.Base.h"
namespace Directive
{
  class ConnectResponse : public Directive::Base
  {
  public:
    static const boost::uint32_t Success = 2000;
    static const boost::uint32_t NameTaken = 2001;
    static const boost::uint32_t OtherError = 2002;

    ConnectResponse();
    ConnectResponse(boost::uint32_t code, const std::string& name);
    virtual ~ConnectResponse();
    std::string toJson() const;
    void fromJson(const std::string& data);
    Directive::Id id() const;
    boost::uint32_t getCode() const;
    const std::string& getName() const;

  protected:
    boost::uint32_t code;
    std::string name;
    const std::string CodeKey = "code";
    const std::string NameKey = "name";
  };
};

#endif // DIRECTIVE_CONNECT_RESPONSE_H
