//==================================================================================================
//    Directive.Connect.cc
//==================================================================================================
#include "Directive.Connect.h"

namespace Directive
{
  //================================================================================================
  ///   Default constructor. We need this to use fromJson(), which we need, because we can't have
  //    two string based constructors.
  //================================================================================================
  Connect::Connect()
  {
  }

  //================================================================================================
  ///   Constructor with a parameter.
  //    @param desiredUserName  The user name to try to acquire.
  //================================================================================================
  Connect::Connect(const std::string& desiredUserName)
  {
    this->desiredUserName = desiredUserName;
  }

  //================================================================================================
  ///   Destructor.
  //================================================================================================
  Connect::~Connect()
  {
  }

  //================================================================================================
  ///   Converts the directive to json format.
  //    @return   The json formatted string.
  //================================================================================================
  std::string Connect::toJson() const
  {
    json::object o;
    o.append(Directive::Base::IdKey, Directive::asStr(this->id()));
    o.append(Directive::Connect::UserNameKey, this->desiredUserName);
    return json::encode(o);
  }

  //================================================================================================
  ///   Converts the directive from json.
  //    @param    json    The json formatted data.
  //================================================================================================
  void Connect::fromJson(const std::string& data)
  {
    json::value j = json::parse(data);
    if (j.is_object())
    {
      if (!j.has_key(Directive::Connect::UserNameKey))
      {
        return;
      }

      json::value name = j[Directive::Connect::UserNameKey];
      if (name.is_string())
      {
        this->desiredUserName = name.to_string();
      }
    }
  }

  //================================================================================================
  ///   Returns the Directive ID.
  //================================================================================================
  Directive::Id Connect::id() const
  {
    return Directive::ConnectId;
  }

  //================================================================================================
  ///   Returns the username.
  //================================================================================================
  const std::string& Connect::getName() const
  {
    return this->desiredUserName;
  }
};
