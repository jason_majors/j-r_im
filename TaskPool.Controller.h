//==================================================================================================
//    TaskPool.Controller.h
//==================================================================================================
#ifndef TASK_POOL_CONTROLLER_H
#define TASK_POOL_CONTROLLER_H
#include "ArgMap.h"
#include "Lock.h"
#include "TaskPool.Task.h"
#include <boost/shared_ptr.hpp>
#include <pthread.h>
#include <queue>
#include <unordered_map>

namespace TaskPool
{
//==================================================================================================
///   Manages the task pool.
//==================================================================================================
class Controller
{
public:
  Controller(const ArgMap::ArgMap& args);
  virtual ~Controller();

  static void* startWorker(void* worker);
  void stop();
  void grow(unsigned int count=1);
  void cull(unsigned int count=1);
  unsigned int workerCount() const;
  unsigned int taskCount() const;
  void enqueueTask(boost::shared_ptr<Task> task);

protected:
  //================================================================================================
  ///   Processes tasks in parallel with other workers.
  //================================================================================================
  class Worker
  {
  public:
    Worker(Controller& controller, int id);
    virtual ~Worker();

    void run();
    void requestExit();
    void join();

  private:
    bool exitRequested;
    Controller& controller;
    pthread_t pthread;
    int id;

    friend class Controller;
  };

  typedef std::vector<boost::shared_ptr<Worker>/**/> WorkerVector;
  typedef std::queue<boost::shared_ptr<Task>/**/> TaskQueue;

  WorkerVector workers;
  TaskQueue tasks;
  Lock::Shared taskQueueLock;

  friend class Worker;
};
};

#endif // TASK_POOL_CONTROLLER_H
