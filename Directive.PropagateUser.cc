//==================================================================================================
//    Directive.PropagateUser.cc
//==================================================================================================
#include "Directive.PropagateUser.h"

namespace Directive
{
  //================================================================================================
  ///   Default constructor. We need this to use fromJson(), which we need, because we can't have
  //    two string based constructors.
  //================================================================================================
  PropagateUser::PropagateUser()
  {
  }

  //================================================================================================
  ///   Constructor with a parameter.
  //    @param desiredUserName  The user name to try to acquire.
  //================================================================================================
  PropagateUser::PropagateUser(const std::string& desiredUserName)
  {
    this->desiredUserName = desiredUserName;
  }

  //================================================================================================
  ///   Destructor.
  //================================================================================================
  PropagateUser::~PropagateUser()
  {
  }

  //================================================================================================
  ///   Converts the directive to json format.
  //    @return   The json formatted string.
  //================================================================================================
  std::string PropagateUser::toJson() const
  {
    json::object o;
    o.append(Directive::Base::IdKey, Directive::asStr(this->id()));
    o.append(Directive::PropagateUser::UserNameKey, this->desiredUserName);
    return json::encode(o);
  }

  //================================================================================================
  ///   Converts the directive from json.
  //    @param    json    The json formatted data.
  //================================================================================================
  void PropagateUser::fromJson(const std::string& data)
  {
    json::value j = json::parse(data);
    if (j.is_object())
    {
      if (!j.has_key(Directive::PropagateUser::UserNameKey))
      {
        return;
      }

      json::value name = j[Directive::PropagateUser::UserNameKey];
      if (name.is_string())
      {
        this->desiredUserName = name.to_string();
      }
    }
  }

  //================================================================================================
  ///   Returns the Directive ID.
  //================================================================================================
  Directive::Id PropagateUser::id() const
  {
    return Directive::PropagateUserId;
  }

  //================================================================================================
  ///   Returns the username.
  //================================================================================================
  const std::string& PropagateUser::getName() const
  {
    return this->desiredUserName;
  }
};
