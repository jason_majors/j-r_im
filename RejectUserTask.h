//==================================================================================================
//    RejectUserTask.h
//==================================================================================================
#ifndef REJECT_USER_TASK_H
#define REJECT_USER_TASK_H
#include <boost/shared_ptr.hpp>
#include "Directive.ConnectResponse.h"
#include "ServerState.h"
#include "SocketThread.h"
#include "TaskPool.Task.h"

//==================================================================================================
///   Handles the work required to reject a user.
//==================================================================================================
class RejectUserTask : public TaskPool::Task
{
public:
  RejectUserTask(const Directive::ConnectResponse& directive,
                 const boost::shared_ptr<SocketThread>& socketThread,
                 const boost::shared_ptr<ServerState>& serverState);
  virtual ~RejectUserTask();

  void process();

protected:
  Directive::ConnectResponse directive;
  boost::shared_ptr<SocketThread> socketThread;
  boost::shared_ptr<ServerState> serverState;
};

#endif // REJECT_USER_TASK_H
