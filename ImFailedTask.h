//==================================================================================================
//    ImFailedTask.h
//==================================================================================================
#ifndef IM_FAILED_TASK_H
#define IM_FAILED_TASK_H
#include <boost/shared_ptr.hpp>
#include "Directive.ImFailed.h"
#include "ServerState.h"
#include "SocketThread.h"
#include "TaskPool.Task.h"

//==================================================================================================
///   Handles the work required to let a user know an IM didn't get through.
//==================================================================================================
class ImFailedTask : public TaskPool::Task
{
public:
  ImFailedTask(const Directive::ImFailed& directive,
               const boost::shared_ptr<ServerState>& serverState);
  virtual ~ImFailedTask();

  void process();

protected:
  Directive::ImFailed directive;
  boost::shared_ptr<ServerState> serverState;
};

#endif // IM_FAILED_TASK_H
