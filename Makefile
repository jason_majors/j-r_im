#===================================================================================================
# Makefile for building the IM client and server.
#===================================================================================================
CC                    = gcc
CXX                   = g++
LINK                  = g++
DOT_A                 = 
LN                    = -ln -s
CXXFLAGS              = $(CPPFLAGS) -std=c++11 -O1
LFLAGS                = $(CPPFLAGS) -pthread
DEBUG                 = -g -O0
LIBS                  = 
                 
                 
CLIENT_TARGET         = imClient
SERVER_TARGET         = imServer
DEBUG_TARGET          = $(SERVER_TARGET).debug $(CLIENT_TARGET).debug
TEST_TARGET           = try

COMMON_SOURCE         = SocketClient.cc SocketBase.cc Exceptions.cc Logger.cc
D                     = Directive
TASKS                 = TaskPool.Controller.cc ReadTask.cc ConnectTask.cc DisconnectTask.cc \
                        SendImTask.cc ListRequestTask.cc
SERVER_TASKS          = $(TASKS) SubscribeTask.cc PropagateUserTask.cc RejectUserTask.cc \
                        UserDisconnectedTask.cc ImFailedTask.cc
DIRECTIVES            = $(D).Base.cc $(D).Connect.cc $(D).ConnectResponse.cc $(D).Disconnect.cc \
                        $(D).UserConnected.cc $(D).ImFailed.cc $(D).SendIm.cc $(D).ListRequest.cc \
                        $(D).UserDisconnected.cc $(D).ListResponse.cc
SERVER_DIRECTIVES     = $(DIRECTIVES) $(D).Subscribe.cc $(D).SubscribeResponse.cc \
                        $(D).PropagateUser.cc
CLIENT_SOURCES        = ThreadedIMClient.cc ClientMain.cc $(DIRECTIVES) $(COMMON_SOURCE)
SERVER_SOURCES        = ServerMain.cc SocketServer.cc $(COMMON_SOURCE) ServerState.cc ArgMap.cc \
                        ThreadedSocketServer.cc $(SERVER_DIRECTIVES) $(SERVER_TASKS)
TEST_SOURCES          = Directive.Connect.cc Directive.Base.cc Directive.ListResponse.cc \
                        Directive.SendIm.cc testMain.cc Directive.Subscribe.cc

DEBUG_CLIENT_OBJECTS  = $(DEBUG_CLIENT_SOURCES:.cc=.od)
DEBUG_SERVER_OBJECTS  = $(DEBUG_SERVER_SOURCES:.cc=.od)
TEST_OBJECTS          = $(TEST_SOURCES:.cc=.o)
CLIENT_OBJECTS        = $(CLIENT_SOURCES:.cc=.o)
SERVER_OBJECTS        = $(SERVER_SOURCES:.cc=.o)


.cc.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

%.od: %.cc
	$(CXX) -c $(CXXFLAGS) $(DEBUG) $(INCPATH) -o "$@" "$<"

all: $(CLIENT_TARGET) $(SERVER_TARGET)

debug: $(DEBUG_TARGET)

test: $(TEST_TARGET)

clean:
	-rm -f core $(CLIENT_TARGET) $(SERVER_TARGET) $(TEST_TARGET) $(DEBUG_TARGET) *.o *.od

$(DEBUG_TARGET): $(DEBUG_CLIENT_TARGET) $(DEBUG_SERVER_TARGET)

$(TEST_TARGET): $(TEST_OBJECTS)
	$(LINK) $(LFLAGS) -o $(TEST_TARGET) $(TEST_OBJECTS) $(OBJCOMP) $(LIBS) $(DOT_A)

$(CLIENT_TARGET): $(CLIENT_OBJECTS)
	$(LINK) $(LFLAGS) -o $(CLIENT_TARGET) $(CLIENT_OBJECTS) $(OBJCOMP) $(LIBS) $(DOT_A)

$(SERVER_TARGET): $(SERVER_OBJECTS)
	$(LINK) $(LFLAGS) -o $(SERVER_TARGET) $(SERVER_OBJECTS) $(OBJCOMP) $(LIBS) $(DOT_A)

$(DEBUG_CLIENT_TARGET): $(DEBUG_CLIENT_OBJECTS)
	$(LINK) $(LFLAGS) -o $(DEBUG_CLIENT_TARGET) $(DEBUG_CLIENT_OBJECTS) $(OBJCOMP) $(LIBS) $(DOT_A)

$(DEBUG_SERVER_TARGET): $(DEBUG_SERVER_OBJECTS)
	$(LINK) $(LFLAGS) -o $(DEBUG_SERVER_TARGET) $(DEBUG_SERVER_OBJECTS) $(OBJCOMP) $(LIBS) $(DOT_A)
