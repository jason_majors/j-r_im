//==================================================================================================
//    Directive.SendIm.cc
//==================================================================================================
#include "Directive.SendIm.h"

namespace Directive
{
  //================================================================================================
  ///   Default constructor.
  //================================================================================================
  SendIm::SendIm()
  {
  }

  //================================================================================================
  ///   Useful constructor.
  //================================================================================================
  SendIm::SendIm(const Directive::ImData& imData) : imData(imData)
  {
  }

  //================================================================================================
  ///   Destructor.
  //================================================================================================
  SendIm::~SendIm()
  {
  }

  //================================================================================================
  ///   Converts to json.
  //================================================================================================
  std::string SendIm::toJson() const
  {
    json::object o;
    o.append(Directive::Base::IdKey, Directive::asStr(this->id()));
    o.append(Directive::SendIm::ImDataStructKey, this->imData.toJson());
    return json::encode(o);
  }

  //================================================================================================
  ///   Converts from json.
  //================================================================================================
  void SendIm::fromJson(const std::string& data)
  {
    json::value j = json::parse(data);
    if (j.is_object())
    {
      if (j.has_key(Directive::SendIm::ImDataStructKey))
      {
        json::value imData = j[Directive::SendIm::ImDataStructKey];
        if (imData.is_object())
        {
          this->imData.fromJson(imData.to_object());
        }
      }
    }
  }

  //================================================================================================
  ///   Returns the id.
  //================================================================================================
  Directive::Id SendIm::id() const
  {
    return Directive::SendImId;
  }

  //================================================================================================
  ///   Gets the name of the IM sender.
  //================================================================================================
  std::string SendIm::getOrigin() const
  {
    return this->imData.origin;
  }

  //================================================================================================
  ///   Gets the name of the IM destination.
  //================================================================================================
  std::string SendIm::getTarget() const
  {
    return this->imData.target;
  }

  //================================================================================================
  ///   Gets the IM message.
  //================================================================================================
  std::string SendIm::getContent() const
  {
    return this->imData.content;
  }
};
