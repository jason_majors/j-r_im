//==================================================================================================
//    SocketClient.cc
//==================================================================================================
#include <arpa/inet.h>
#include <cerrno>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <stdexcept>
#include <sys/socket.h>
#include <sys/types.h>
#include "Exceptions.h"
#include "SocketClient.h"

//==================================================================================================
///   Constructor.
//    @param  hostname  The host to connect to.
//    @param  port      The port to connect to.
//    @param  type      The type of connection to create (pass through to SocketBase).
//==================================================================================================
SocketClient::SocketClient(const std::string& hostname, int port, SocketBase::SocketType type,
                           bool closeOnDestruct)
: SocketBase(type, closeOnDestruct), hostname(hostname), port(port)
{
}

//==================================================================================================
///   Destructor.
//==================================================================================================
SocketClient::~SocketClient()
{
}

//==================================================================================================
///   Opens the connection to the host.
//    @throws std::runtime_error If a system call fails.
//==================================================================================================
void SocketClient::connect()
{
  struct hostent* hostEntity = ::gethostbyname(this->hostname.c_str());
  if (!hostEntity)
  {
    Exceptions::raise<Exceptions::ConnectException>(__FILE__, __LINE__, ::strerror(errno));
  }

  struct sockaddr_in sockAddr;
  ::memset(&sockAddr, 0, sizeof(sockaddr_in));
  sockAddr.sin_family = AF_INET;
  sockAddr.sin_port = htons(this->port); // ::htons is an error with -O2, gcc must optimize it.
  ::memcpy(&sockAddr.sin_addr.s_addr, hostEntity->h_addr_list[0], hostEntity->h_length);
  std::string hostIp(inet_ntoa(*(struct in_addr*)hostEntity->h_addr));
  this->socket = ::socket(AF_INET, this->type, 0);
  if (this->socket < 0)
  {
    Exceptions::raise<Exceptions::ConnectException>(__FILE__, __LINE__, ::strerror(errno));
  }

  int result = ::connect(this->socket, (struct sockaddr*)&sockAddr, sizeof(sockaddr_in));
  if (result < 0)
  {
    Exceptions::raise<Exceptions::ConnectException>(__FILE__, __LINE__, ::strerror(errno));
  }
}

//==================================================================================================
///   Reopens the connection to the host.
//    @throws std::runtime_error If a system call fails.
//==================================================================================================
void SocketClient::reconnect()
{
  this->disconnect(false);
  this->connect();
}
