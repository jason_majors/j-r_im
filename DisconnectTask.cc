//==================================================================================================
//    DisconnectTask.cc
//==================================================================================================
#include "Logger.h"
#include "DisconnectTask.h"
#include "Directive.UserDisconnected.h"

//==================================================================================================
///   Constructor.
//==================================================================================================
DisconnectTask::DisconnectTask(const Directive::Disconnect& directive,
                               const boost::shared_ptr<SocketThread>& socketThread,
                               const boost::shared_ptr<ServerState>& serverState)
: directive(directive), socketThread(socketThread), serverState(serverState)
{
}

//==================================================================================================
///   Destructor.
//==================================================================================================
DisconnectTask::~DisconnectTask()
{
}

//==================================================================================================
///   The server directly connected to a client does this when the client disconnects.
//==================================================================================================
void DisconnectTask::process()
{
  Logger::getLogger()->info("DisconnectTask: " + this->directive.toJson());

  // Write the UserDisconnected directive to every connected node, except the originator.
  std::string user = this->serverState->getUserOnSocket(this->socketThread);
  if (user.length() != 0)
  {
    Logger::getLogger()->info("DisconnectTask: disconnecting user: " + user + ".");
    boost::shared_ptr<Directive::Base> d =
      boost::shared_ptr<Directive::Base>(new Directive::UserDisconnected(user));
    this->serverState->removeUser(user);
    this->serverState->writeDirectiveToOtherClients(d, user);
    this->serverState->writeDirectiveToOtherServers(d, this->socketThread);
  }
}
