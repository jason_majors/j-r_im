//==================================================================================================
//    Directive.Subscribe.cc
//==================================================================================================
#include "Directive.Subscribe.h"

namespace Directive
{
  //================================================================================================
  ///   Default constructor.
  //================================================================================================
  Subscribe::Subscribe()
  {
  }

  //================================================================================================
  ///   Constructor with a parameter.
  //    @param server   The server trying to subscribe.
  //================================================================================================
  Subscribe::Subscribe(const Directive::Server& server)
  {
    this->server = server;
  }

  //================================================================================================
  ///   Destructor.
  //================================================================================================
  Subscribe::~Subscribe()
  {
  }

  //================================================================================================
  ///   Converts the directive to json format.
  //    @return   The json formatted string.
  //================================================================================================
  std::string Subscribe::toJson() const
  {
    json::object o;
    o.append(Directive::Base::IdKey, Directive::asStr(this->id()));
    o.append(Directive::Subscribe::ServerInfoKey, this->server.toJson());
    return json::encode(o);
  }

  //================================================================================================
  ///   Converts the directive from json.
  //    @param    json    The json formatted data.
  //================================================================================================
  void Subscribe::fromJson(const std::string& data)
  {
    json::value j = json::parse(data);
    if (j.is_object())
    {
      if (!j.has_key(Directive::Subscribe::ServerInfoKey))
      {
        return;
      }

      json::value server = j[Directive::Subscribe::ServerInfoKey];
      if (server.is_object())
      {
        Directive::Server s;
        s.fromJson(server.to_object());
        this->server = s;
      }
    }
  }

  //================================================================================================
  ///   Returns the Directive ID.
  //================================================================================================
  Directive::Id Subscribe::id() const
  {
    return Directive::SubscribeId;
  }

  //================================================================================================
  ///   Returns the server.
  //================================================================================================
  const Directive::Server& Subscribe::getServer() const
  {
    return this->server;
  }
};
