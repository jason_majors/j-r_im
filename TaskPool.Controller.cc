//==================================================================================================
//    TaskPool.Controller.cc
//==================================================================================================
#include "TaskPool.Controller.h"
#include <algorithm>
#include <unistd.h>

namespace TaskPool
{
//==================================================================================================
///   Constructor.
//    @param  controller  The TaskPool::Controller that controls this worker.
//==================================================================================================
Controller::Worker::Worker(Controller& controller, int id) : controller(controller), id(id)
{
  this->exitRequested = false;
}

//==================================================================================================
///   Destructor.
//==================================================================================================
Controller::Worker::~Worker()
{
}

//==================================================================================================
///   Does the work. Loops until told to stop, pulling tasks off the queue and doing them.
//==================================================================================================
void Controller::Worker::run()
{
  while (true)
  {
    this->controller.taskQueueLock.acquire();
    if (this->controller.tasks.size() > 0)
    {
      boost::shared_ptr<Task> task = this->controller.tasks.front();
      this->controller.tasks.pop();
      this->controller.taskQueueLock.release();
      task->process();
    }
    else if (this->exitRequested)
    {
      this->controller.taskQueueLock.release();
      return;
    }
    else
    {
      this->controller.taskQueueLock.release();
      ::usleep(100);
    }
  }
}

//==================================================================================================
///   Requests that the worker exit after its current task.
//==================================================================================================
void Controller::Worker::requestExit()
{
  this->exitRequested = true;
}

//==================================================================================================
///   Blocks until the worker 'retires'.
//==================================================================================================
void Controller::Worker::join()
{
  ::pthread_join(this->pthread, nullptr);
}

//==================================================================================================
///   Constructor.
//    @param  workerCount   The initial number of workers.
//==================================================================================================
Controller::Controller(const ArgMap::ArgMap& args)
{
  this->workers.reserve(args.at(ArgMap::ThreadPoolSizeKey).toLong());
  this->grow(args.at(ArgMap::ThreadPoolSizeKey).toLong());
}

//==================================================================================================
///   Destructor.
//==================================================================================================
Controller::~Controller()
{
  this->stop();
}

//==================================================================================================
///   Intermediary to be able to pass a function pointer to pthread_create. Calls worker->run().
//    @param  controller  The worker to start.
//==================================================================================================
void* Controller::startWorker(void* worker)
{
  Worker* w = static_cast<Worker*>(worker);
  boost::shared_ptr<Worker> newWorker(w);
  newWorker->controller.workers.push_back(newWorker);
  newWorker->run();
}

//==================================================================================================
///   Stops the threads and joins them.
//==================================================================================================
void Controller::stop()
{
  this->cull(this->workerCount());
}

//==================================================================================================
///   Increases the number of workers in the pool.
//    I have to pass the heap pointer across and create the shared_ptr in startWorker(), because
//    passing a pointer to a local shared_ptr would cause a race condition, when the local goes
//    out of scope in the next iteration.
//    @param  count   The number of new workers to add.
//==================================================================================================
void Controller::grow(unsigned int count)
{
  for (int i = 0; i < count; i++)
  {
    Worker* worker = new Worker(*this, i);
    ::pthread_create(&worker->pthread, nullptr, startWorker, (void*)worker);
  }
}

//==================================================================================================
///   Decreases the number of workers in the pool. Rather than trying to determine which are busy
//    and which are idle, it just chops the last n off the vector. It does wait for them to
//    finish any current work.
//    @param  count   The number of workers to remove.
//==================================================================================================
void Controller::cull(unsigned int count)
{
  int cullCount = std::min((int)count, (int)this->workers.size());
  int start = this->workers.size() - cullCount;
  int stop = this->workers.size();

  for (int i = start; i < stop; i++)
  {
    if (this->workers[i])
    {
      this->workers[i]->requestExit();
    }
  }

  for (int i = 0; i < cullCount; i++)
  {
    boost::shared_ptr<Worker> w = workers.back();
    workers.pop_back();
    w->join();
    std::cerr << "Joined " << i + 1 << " / " << cullCount << " threads..." << std::endl;
  }
}

//==================================================================================================
///   Returns the number of queued tasks.
//    @return The number of queued tasks.
//==================================================================================================
unsigned int Controller::taskCount() const
{
  return this->tasks.size();
}

//==================================================================================================
///   Returns the number of workers available for tasks.
//    @return The number of workers available for tasks.
//==================================================================================================
unsigned int Controller::workerCount() const
{
  return this->workers.size();
}

//==================================================================================================
///   Adds a task to the back of the queue.
//    @param  task  The task object to enqueue.
//==================================================================================================
void Controller::enqueueTask(boost::shared_ptr<Task> task)
{
  this->taskQueueLock.acquire();
  this->tasks.push(task);
  this->taskQueueLock.release();
}
};
