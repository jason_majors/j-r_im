//==================================================================================================
//    ArgMap.cc
//==================================================================================================
#include <cstdlib>
#include <sstream>
#include "ArgMap.h"
#include "Exceptions.h"

namespace ArgMap
{
  //================================================================================================
  ///   Constructor.
  //================================================================================================
  ArgumentException::ArgumentException(const std::string& what) : std::runtime_error(what)
  {
  }

  //================================================================================================
  ///   Default constructor. Required by the std::unordered_map [] operator.
  //================================================================================================
  Arg::Arg()
  {
  }

  //================================================================================================
  ///   Constructs from a long.
  //    @param  a   A long.
  //================================================================================================
  Arg::Arg(long a)
  {
    std::stringstream s;
    s << a << std::ends;
    this->value = s.str();
  }

  //================================================================================================
  ///   Constructs from a string.
  //    @param  a   A string.
  //================================================================================================
  Arg::Arg(const std::string& a) : value(a)
  {
  }

  //================================================================================================
  ///   Destructor.
  //================================================================================================
  Arg::~Arg()
  {
  }

  //================================================================================================
  ///   Returns the arg as a long.
  //================================================================================================
  long Arg::toLong() const
  {
    return ::strtol(this->value.c_str(), nullptr, 10);
  }

  //================================================================================================
  ///   Returns the arg as a string.
  //================================================================================================
  const std::string& Arg::toString() const
  {
    return this->value;
  }

  //================================================================================================
  ///   Parses the command line arguments into a map.
  //    @param  argc    The argc variable from main().
  //    @param  argv    The argv variable from main().
  //    @param  args    A string->Arg map.
  //    @throw  ArgumentException if there's a parsing issue.
  //================================================================================================
  void parseArgs(int argc, char* argv[], ArgMap& args)
  {
    for (int i = 1; i < argc; i++)
    {
      std::string s(argv[i]);
      int equalIndex = s.find('=');
      if (equalIndex == std::string::npos)
      {
        Exceptions::raise<ArgumentException>(__FILE__, __LINE__, s);
      }

      args[s.substr(0, equalIndex)] = s.substr(equalIndex + 1);
    }
  }
};
