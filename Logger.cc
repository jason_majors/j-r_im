//==================================================================================================
//    Logger.cc
//==================================================================================================
#include <boost/date_time.hpp>
#include <boost/date_time/local_time/local_time.hpp>
#include <iostream>
#include <sstream>
#include "Logger.h"

boost::shared_ptr<Logger> Logger::logger = boost::shared_ptr<Logger>((Logger*)0);
Lock::Shared Logger::lock;
//==================================================================================================
///   Returns the static logger instance, after creating it, if necessary.
//==================================================================================================
boost::shared_ptr<Logger> Logger::getLogger()
{
  Lock::Scoped l(Logger::lock);
  if (!Logger::logger)
  {
    Logger::logger = boost::shared_ptr<Logger>(new Logger);
  }

  return Logger::logger;
}

//==================================================================================================
///   Writes a message to any active logs. Returns whether the message is urgent enough to log. 
//==================================================================================================
bool Logger::log(Logger::Level messageLevel, const std::string& logMessage)
{
  Lock::Scoped l(this->lock);
  if (messageLevel >= this->logLevel)
  {
    boost::local_time::local_date_time t =
      boost::local_time::local_sec_clock::local_time(boost::local_time::time_zone_ptr());
    std::stringstream formattedMessage;
    formattedMessage << "[" << t << "] " << Logger::stringify(messageLevel) << logMessage
                     << std::endl;
    if (this->useStderr)
    {
      std::cerr << formattedMessage.str();
    }
    
    if (this->outFileStream)
    {
      this->outFileStream->write(formattedMessage.str().c_str(), formattedMessage.str().length());
    }

    return true;
  }

  return false;
}

//==================================================================================================
///   Writes info.
//==================================================================================================
bool Logger::info(const std::string& logMessage)
{
  return this->log(Logger::Info, logMessage);
}

//==================================================================================================
///   Writes a warning.
//==================================================================================================
bool Logger::warn(const std::string& logMessage)
{
  return this->log(Logger::Warn, logMessage);
}

//==================================================================================================
///   Writes an error.
//==================================================================================================
bool Logger::error(const std::string& logMessage)
{
  return this->log(Logger::Error, logMessage);
}

//==================================================================================================
///   Toggles the use of stderr.
//==================================================================================================
void Logger::logToStderr(bool doit)
{
  Lock::Scoped l(this->lock);
  this->useStderr = doit;
}

//==================================================================================================
///   (De)activates logging to a file. Pass an empty string to deactivate it.
//==================================================================================================
void Logger::logToFile(const std::string& fileName)
{
  Lock::Scoped l(this->lock);
  this->fileName = fileName;
  if (this->outFileStream && this->fileName.length() == 0)
  {
    this->outFileStream = boost::shared_ptr<std::ofstream>((std::ofstream*)0);
  }
  else
  {
    this->outFileStream =
      boost::shared_ptr<std::ofstream>(new std::ofstream(this->fileName.c_str()));
  }
}

//==================================================================================================
///   Sets the log level.
//==================================================================================================
void Logger::setLogLevel(Logger::Level level)
{
  Lock::Scoped l(this->lock);
  if (level == Logger::Info || level == Logger::Warn || level == Logger::Error)
  {
    this->logLevel = level;
  }
}

//==================================================================================================
///   Returns the name of the current log file.
//==================================================================================================
std::string Logger::logFileName()
{
  Lock::Scoped l(this->lock);
  return this->fileName;
}

//==================================================================================================
///   Returns the log level as a string.
//==================================================================================================
std::string Logger::stringify(Logger::Level level)
{
  switch (level)
  {
  case Logger::Info:
    return "Info:  ";
  case Logger::Warn:
    return "Warn:  ";
  case Logger::Error:
    return "Error: ";
  default:
    return "????:  ";
  }
}

//==================================================================================================
///   Private constructor.
//==================================================================================================
Logger::Logger()
{
  this->logLevel = Logger::Info;
  this->useStderr = true;
}

//==================================================================================================
///   Don't use the copy constructor!
//==================================================================================================
Logger::Logger(const Logger& l)
{
}
