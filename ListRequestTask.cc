//==================================================================================================
//    ListRequestTask.cc
//==================================================================================================
#include "Exceptions.h"
#include "Logger.h"
#include "ListRequestTask.h"
#include "Directive.ListResponse.h"

//==================================================================================================
///   Constructor.
//==================================================================================================
ListRequestTask::ListRequestTask(const Directive::ListRequest& directive,
                         const boost::shared_ptr<SocketThread>& socketThread,
                         const boost::shared_ptr<ServerState>& serverState)
: directive(directive), socketThread(socketThread), serverState(serverState)
{
}

//==================================================================================================
///   Destructor.
//==================================================================================================
ListRequestTask::~ListRequestTask()
{
}

//==================================================================================================
///   Called when the task starts.
//==================================================================================================
void ListRequestTask::process()
{
  try
  {
    Logger::getLogger()->info("ListRequestTask: " + this->directive.toJson());
    Directive::ListResponse d(this->serverState->getUserNames());
    std::string responseJson = d.toJson();
    responseJson.append("\r\n");
    this->socketThread->write(responseJson);
  }
  catch(Exceptions::WriteException& e)
  {
    Logger::getLogger()->error("ListRequestTask: Caught WriteException:");
    Logger::getLogger()->error(e.what());
  }
}
