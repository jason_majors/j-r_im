//==================================================================================================
//    ThreadedSocketServer.h
//==================================================================================================
#ifndef THREADED_SOCKET_SERVER_H
#define THREADED_SOCKET_SERVER_H
#include "ArgMap.h"
#include "Lock.h"
#include "ServerState.h"
#include "SocketServer.h"
#include "TaskPool.Controller.h"
//==================================================================================================
///   Abstracts the connecting, accepting, reading, writing, and disconnecting.
//==================================================================================================
class ThreadedSocketServer : public SocketServer
{
public:
  ThreadedSocketServer(SocketBase::SocketType type, const ArgMap::ArgMap& args);
  ThreadedSocketServer();
  virtual ~ThreadedSocketServer();
  virtual void runLoop();
  void stop();

protected:
  void subscribe(const std::string& host, long port);
  TaskPool::Controller taskController;
  const ArgMap::ArgMap& args;
  boost::shared_ptr<ServerState> serverState;
};

#endif // THREADED_SOCKET_SERVER_H
