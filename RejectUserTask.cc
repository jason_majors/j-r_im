//==================================================================================================
//    RejectUserTask.cc
//==================================================================================================
#include "Exceptions.h"
#include "Logger.h"
#include "RejectUserTask.h"

static const std::string CRLF = "\r\n";
//==================================================================================================
///   Constructor.
//==================================================================================================
RejectUserTask::RejectUserTask(const Directive::ConnectResponse& directive,
                         const boost::shared_ptr<SocketThread>& socketThread,
                         const boost::shared_ptr<ServerState>& serverState)
: directive(directive), socketThread(socketThread), serverState(serverState)
{
}

//==================================================================================================
///   Destructor.
//==================================================================================================
RejectUserTask::~RejectUserTask()
{
}

//==================================================================================================
///   Passes the rejection along and removes the user from the map. If the user is already gone,
//    ignore it.
//==================================================================================================
void RejectUserTask::process()
{
  try
  {
    Logger::getLogger()->info("RejectUserTask: " + this->directive.toJson());
    boost::shared_ptr<SocketThread> route =
      this->serverState->routeToUser(this->directive.getName());
    if (route)
    {
      route->write(this->directive.toJson() + CRLF);
      this->serverState->removeUser(this->directive.getName());
    }
  }
  catch(Exceptions::WriteException& e)
  {
    Logger::getLogger()->error("RejectUserTask: Caught WriteException:");
    Logger::getLogger()->error(e.what());
  }
}
