//==================================================================================================
//    Directive.ImFailed.cc
//==================================================================================================
#include "Directive.ImFailed.h"

namespace Directive
{
  //================================================================================================
  ///   Default constructor. We need this to use fromJson(), which we need, because we can't have
  //    two string based constructors.
  //================================================================================================
  ImFailed::ImFailed()
  {
  }

  //================================================================================================
  ///   Constructor with a parameter.
  //    @param target  The unreachable user name.
  //    @param origin  The sending user name.
  //================================================================================================
  ImFailed::ImFailed(const std::string& target, const std::string& origin)
  : target(target), origin(origin)
  {
  }

  //================================================================================================
  ///   Destructor.
  //================================================================================================
  ImFailed::~ImFailed()
  {
  }

  //================================================================================================
  ///   Converts the directive to json format.
  //    @return   The json formatted string.
  //================================================================================================
  std::string ImFailed::toJson() const
  {
    json::object o;
    o.append(Directive::Base::IdKey, Directive::asStr(this->id()));
    o.append(Directive::ImFailed::TargetKey, this->target);
    o.append(Directive::ImFailed::OriginKey, this->origin);
    return json::encode(o);
  }

  //================================================================================================
  ///   Converts the directive from json.
  //    @param    json    The json formatted data.
  //================================================================================================
  void ImFailed::fromJson(const std::string& data)
  {
    json::value j = json::parse(data);
    if (j.is_object())
    {
      if (!j.has_key(Directive::ImFailed::OriginKey) || !j.has_key(Directive::ImFailed::OriginKey))
      {
        return;
      }

      json::value origin = j[Directive::ImFailed::OriginKey];
      if (origin.is_string())
      {
        this->origin = origin.to_string();
      }

      json::value target = j[Directive::ImFailed::TargetKey];
      if (target.is_string())
      {
        this->target = target.to_string();
      }
    }
  }

  //================================================================================================
  ///   Returns the Directive ID.
  //================================================================================================
  Directive::Id ImFailed::id() const
  {
    return Directive::ImFailedId;
  }

  //================================================================================================
  ///   Returns the name of the target.
  //================================================================================================
  const std::string& ImFailed::getTarget() const
  {
    return this->target;
  }

  //================================================================================================
  ///   Returns the name of the sender.
  //================================================================================================
  const std::string& ImFailed::getOrigin() const
  {
    return this->origin;
  }
};
