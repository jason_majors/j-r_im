//==================================================================================================
//    UserDisconnectedTask.cc
//==================================================================================================
#include "Logger.h"
#include "UserDisconnectedTask.h"
#include "Directive.UserDisconnected.h"

//==================================================================================================
///   Constructor.
//==================================================================================================
UserDisconnectedTask::UserDisconnectedTask(const Directive::UserDisconnected& directive,
                                           const boost::shared_ptr<SocketThread>& socketThread,
                                           const boost::shared_ptr<ServerState>& serverState)
: directive(directive), socketThread(socketThread), serverState(serverState)
{
}

//==================================================================================================
///   Destructor.
//==================================================================================================
UserDisconnectedTask::~UserDisconnectedTask()
{
}

//==================================================================================================
///   The server directly connected to a client does this when the client disconnects.
//==================================================================================================
void UserDisconnectedTask::process()
{
  Logger::getLogger()->info("UserDisconnectedTask: " + this->directive.toJson());

  // Write the UserDisconnected directive to every connected client and to every connected
  // server, except the originator.
  Logger::getLogger()->info("UserDisconnectedTask: disconnecting user: " +
                            this->directive.getName() + ".");
  boost::shared_ptr<Directive::Base> d =
    boost::shared_ptr<Directive::Base>(new Directive::UserDisconnected(this->directive.getName()));
  this->serverState->writeDirectiveToOtherClients(d, "");
  this->serverState->writeDirectiveToOtherServers(d, this->socketThread);
  this->serverState->removeUser(this->directive.getName());
}
