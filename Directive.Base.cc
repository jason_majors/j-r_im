//==================================================================================================
//    Directive.Base.cc
//==================================================================================================
#include <cstdlib>
#include <sstream>
#include "Directive.Base.h"

const std::string Directive::Base::IdKey = "id";
namespace Directive
{
  //================================================================================================
  ///   Constructor.
  //================================================================================================
  Server::Server(const std::string host, boost::uint32_t port) : host(host), port(port)
  {
  }

  //================================================================================================
  ///   Copy Constructor.
  //================================================================================================
  Server::Server(const Server& rhs) : host(rhs.host), port(rhs.port)
  {
  }

  //================================================================================================
  ///   Converts a server struct to json data.
  //================================================================================================
  json::object Server::toJson() const
  {
    json::object o;
    o.append(Directive::Server::HostKey, this->host);
    o.append(Directive::Server::PortKey, Directive::asStr(this->port));
    return o;
  }

  //================================================================================================
  ///   Reads a server struct from json data.
  //================================================================================================
  void Server::fromJson(const json::object& jsonData)
  {
    if (jsonData.has_key(Directive::Server::HostKey) &&
        jsonData.has_key(Directive::Server::PortKey))
    {
      json::value host = jsonData[Directive::Server::HostKey];
      json::value port = jsonData[Directive::Server::PortKey];
      if (host.is_string() && port.is_string())
      {
        this->host = host.to_string();
        this->port = Directive::asInt(port.to_string());
      }
    }
  }

  //================================================================================================
  ///   Assignment operator.
  //================================================================================================
  const Server& Server::operator=(const Server& rhs)
  {
    this->port = rhs.port;
    this->host = rhs.host;
    return rhs;
  }

  //================================================================================================
  ///   Copy Constructor.
  //================================================================================================
  ImData::ImData(const ImData& rhs) : origin(rhs.origin), target(rhs.target), content(rhs.content)
  {
  }

  //================================================================================================
  ///   Converts an IM Data struct to json data.
  //================================================================================================
  json::object ImData::toJson() const
  {
    json::object o;
    o.append(Directive::ImData::OriginKey, this->origin);
    o.append(Directive::ImData::TargetKey, this->target);
    o.append(Directive::ImData::ContentKey, this->content);
    return o;
  }

  //================================================================================================
  ///   Reads an IM Data struct from json data.
  //================================================================================================
  void ImData::fromJson(const json::object& jsonData)
  {
    if (jsonData.has_key(Directive::ImData::OriginKey) &&
        jsonData.has_key(Directive::ImData::TargetKey) &&
        jsonData.has_key(Directive::ImData::ContentKey))
    {
      json::value origin = jsonData[Directive::ImData::OriginKey];
      json::value target = jsonData[Directive::ImData::TargetKey];
      json::value content = jsonData[Directive::ImData::ContentKey];
      if (content.is_string() && origin.is_string() && target.is_string())
      {
        this->content = content.to_string();
        this->origin = origin.to_string();
        this->target = target.to_string();
      }
    }
  }

  //================================================================================================
  ///   Assignment operator.
  //================================================================================================
  const ImData& ImData::operator=(const ImData& rhs)
  {
    this->origin = rhs.origin;
    this->target = rhs.target;
    this->content = rhs.content;
    return rhs;
  }

  //================================================================================================
  ///   Converts a string to an int.
  //================================================================================================
  boost::uint32_t asInt(const std::string& s)
  {
    return static_cast<boost::uint32_t>(::strtol(s.c_str(), NULL, 10));
  }

  //================================================================================================
  ///   Converts an int to a string.
  //================================================================================================
  std::string asStr(boost::uint32_t i)
  {
    std::stringstream s;
    s << i << std::ends;
    return s.str();
  }

  //================================================================================================
  ///   Default constructor.
  //================================================================================================
  Base::Base()
  {
  }

  //================================================================================================
  ///   Default virtual destructor.
  //================================================================================================
  Base::~Base()
  {
  }

  //================================================================================================
  ///   Pulls the ID out of json string, or None if it fails.
  //================================================================================================
  Id Base::parseId(const std::string& s)
  {
    json::value j = json::parse(s);
    if (j.is_object() && j.has_key(Directive::Base::IdKey))
    {
      return Directive::asInt(j[Directive::Base::IdKey].to_string());
    }

    return Directive::NoneId;
  }
};
