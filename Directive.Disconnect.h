//==================================================================================================
//    Directive.Disconnect.h
//==================================================================================================
#ifndef DIRECTIVE_DISCONNECT_H
#define DIRECTIVE_DISCONNECT_H
#include "Directive.Base.h"

namespace Directive
{
  //================================================================================================
  ///   Directive for when a client disconnects from a server.
  //================================================================================================
  class Disconnect : public Directive::Base
  {
  public:
    Disconnect();
    virtual ~Disconnect();
    std::string toJson() const;
    void fromJson(const std::string& data);
    Directive::Id id() const;
  };
};

#endif // DIRECTIVE_DISCONNECT_H
