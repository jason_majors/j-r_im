//==================================================================================================
//    ThreadedIMClient.cc
//==================================================================================================
#include "ThreadedIMClient.h"
#include "Directive.Connect.h"
#include "Directive.ConnectResponse.h"
#include "Directive.Disconnect.h"
#include "Directive.ImFailed.h"
#include "Directive.ListRequest.h"
#include "Directive.ListResponse.h"
#include "Directive.SendIm.h"
#include "Directive.UserConnected.h"
#include "Directive.UserDisconnected.h"
#include <iostream>
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <climits>

//==================================================================================================
///   Constructor.
//==================================================================================================
ThreadedIMClient::SharedInterfaces::SharedInterfaces()
{
  keepAlive = true;
  lastReceivedUser = boost::shared_ptr<std::string>(new std::string);
  lastSentUsers = boost::shared_ptr<std::vector<std::string>/**/>( new std::vector<std::string>() );
}

//==================================================================================================
///   Destructor.
//==================================================================================================
ThreadedIMClient::SharedInterfaces::~SharedInterfaces()
{
}

//==================================================================================================
///   Resets the existing socket connection with a new connection
//    @param  hostname  The hostname of the new server to connect to.
//    @param  port      The port number on the new server to connect to.
//==================================================================================================
void ThreadedIMClient::SharedInterfaces::connectToServer(const std::string& hostname, int port)
{
  if ( this->isConnectedToServer() ) 
  {
    this->disconnectFromServer();
  }
  Lock::Scoped socketLock(this->networkIoLock);
  this->serverSocket.reset(new SocketClient(hostname, port, SocketBase::Tcp));
  this->serverSocket->connect();
}

//==================================================================================================
///   Disconnects the existing socket connection with the IM Server.
//==================================================================================================
void ThreadedIMClient::SharedInterfaces::disconnectFromServer()
{
  Lock::Scoped socketLock(this->networkIoLock);
  this->serverSocket->disconnect();
}

//==================================================================================================
///   Tests if this ThreadedIMClient instance is currently connected to an IM Server.
//    @return true/false
//==================================================================================================
bool ThreadedIMClient::SharedInterfaces::isConnectedToServer()
{
  Lock::Scoped socketLock(this->networkIoLock);
  return ( this->serverSocket && this->serverSocket->isConnected() );
}

//==================================================================================================
///   Check if the program should still keep looking for input from console, or network socket.
//    @return true/false
//==================================================================================================
bool ThreadedIMClient::SharedInterfaces::checkKeepAlive()
{
  Lock::Scoped runLock(this->keepAliveLock);
  bool retVal = this->keepAlive;
  return retVal;
}

//==================================================================================================
///   Signal all threads listening for network input that it is time to stop.
//==================================================================================================
void ThreadedIMClient::SharedInterfaces::disableKeepAlive()
{
  Lock::Scoped runLock(this->keepAliveLock);
  this->keepAlive = false;
}

//==================================================================================================
///   Signal all threads listening for network input that it is ok to keep listening.
//==================================================================================================
void ThreadedIMClient::SharedInterfaces::enableKeepAlive()
{
  Lock::Scoped runLock(this->keepAliveLock);
  this->keepAlive = true;
}

//==================================================================================================
///   Update the value of the last user to send this client an IM message safely.
//    @param  userName     THe new value of the user name to send this client an IM message.
//==================================================================================================
void ThreadedIMClient::SharedInterfaces::updateLastReceivedUser(const std::string& userName)
{
  Lock::Scoped updateLastUser(this->lastReceivedUserLock);
  this->lastReceivedUser.reset( new std::string(userName) );
}

//==================================================================================================
///   Check the value of the last user to send this client an IM message safely.
//    @return The current user value stored in the last received user variable (may be empty).
//==================================================================================================
const std::string& ThreadedIMClient::SharedInterfaces::checkLastReceivedUser()
{
  Lock::Scoped checkLastUser(this->lastReceivedUserLock);
  return *(this->lastReceivedUser);
}

//==================================================================================================
///   Clears out all user names stored in the last sent users vector.
//==================================================================================================
void ThreadedIMClient::SharedInterfaces::clearLastSentUsers()
{
  Lock::Scoped updateLastSentUsers(this->lastSentUsersLock);
  this->lastSentUsers->clear();
}

//==================================================================================================
///   Adds a user to the last sent users vector, unless the name already exists in the vector.
//    @param  userName     The name of the user to add to the last sent users vector.
//==================================================================================================
void ThreadedIMClient::SharedInterfaces::addToLastSentUsers(const std::string& userName)
{
  Lock::Scoped updateLastSentUsers(this->lastSentUsersLock);
  for (std::vector<std::string>::const_iterator iter = this->lastSentUsers->begin();
       iter != this->lastSentUsers->end(); iter++)
  {
    if (userName.compare(*iter) == 0)
    {
      return;
    }
  }
  this->lastSentUsers->push_back(userName);
}

//==================================================================================================
///   Removes a user from the last sent users vector, if the user exists in the vector.
//    @param  userName     The name of the user to remove from the last sent users vector.
//==================================================================================================
void ThreadedIMClient::SharedInterfaces::removeFromLastSentUsers(const std::string& userName)
{
  Lock::Scoped updateLastSentUsers(this->lastSentUsersLock);
  for (std::vector<std::string>::iterator iter = this->lastSentUsers->begin();
       iter != this->lastSentUsers->end(); iter++) 
  {
    if (userName.compare(*iter) == 0)
    {
      this->lastSentUsers->erase(iter);
      return;
    }
  }
}

//==================================================================================================
///   Returns the last sent users vector.
//    @return the last sent users vector.
//==================================================================================================
const std::vector<std::string>& ThreadedIMClient::SharedInterfaces::getLastSentUsers()
{
  Lock::Scoped getLastSentUsers(this->lastSentUsersLock);
  return *(this->lastSentUsers);
}

//==================================================================================================
///   Look to see if there is any input available on stdin.
//    @param  timeoutSec     Number of seconds to wait before timing out on the select call.
//    @return 0 if no, non-zero if yes.
//==================================================================================================
int ThreadedIMClient::SharedInterfaces::checkConsoleInput(int timeoutSec)
{
  return ( SocketClient::selectReadOnly(0, timeoutSec) );
}

//==================================================================================================
///   Look to see if there is any input available on the connected network socket (if connected).
//    @param  timeoutSec     Number of seconds to wait before timing out on the select call.
//    @return 0 if no, non-zero if yes.
//==================================================================================================
int ThreadedIMClient::SharedInterfaces::checkNetworkInput(int timeoutSec)
{
  //Don't bother looking if we aren't connected to a server...
  if ( !this->serverSocket->isConnected() ) 
  {
    return 0;
  } 
  return ( SocketClient::selectReadOnly(this->serverSocket->socketId(), timeoutSec) );
}

//==================================================================================================
///   Writes data to the connected server socket.
//    @param  data  The data to write to the server.
//==================================================================================================
void ThreadedIMClient::SharedInterfaces::writeToServer(const std::string& data)
{
  Lock::Scoped dontDisconnectMe(networkIoLock);
  try
  {
    this->serverSocket->write(data);
  }
  catch (std::runtime_error& e)
  {
    this->printToConsole("ERROR:  Failure writing Directive message to server!!!!!");
  }
}

//==================================================================================================
///   Reads data from the connected server socket.
//    @return A pointer to the string of bytes, or NULL if reading caused an exception.
//==================================================================================================
boost::shared_ptr<std::string> ThreadedIMClient::SharedInterfaces::readFromServer()
{
  Lock::Scoped dontDisconnectMe(this->networkIoLock);
  boost::shared_ptr<std::string> data;
  try
  {
    data = this->serverSocket->read();
  }
  catch (std::runtime_error& e)
  {
    this->printToConsole("ERROR:  Failure reading Directive message from server!!!!!");
  }
  return data;
}

//==================================================================================================
///   Print a line to stdout after acquiring appropriate lock.
//    @param  line     Line of output to print to stdout
//==================================================================================================
void ThreadedIMClient::SharedInterfaces::printToConsole(const std::string& line)
{
  Lock::Scoped coutLock(this->consoleIoLock);
  std::cout << line << std::endl << std::endl;
}

//==================================================================================================
///   Constructor.
//==================================================================================================
ThreadedIMClient::ThreadedIMClient() 
{
  sharedInterfaces = boost::shared_ptr<SharedInterfaces>(new SharedInterfaces());
  networkThreadId = -1;
  getConsoleInput = true;
  myUserName = boost::shared_ptr<std::string>(new std::string());
}

//==================================================================================================
///   Destructor.
//==================================================================================================
ThreadedIMClient::~ThreadedIMClient() 
{
}

//==================================================================================================
///   Main function for spawned network monitoring thread.  Monitors network socket for Directives
//    from IM Server and acts on them accordingly.
//    @param  threadArgs     boost::shared_ptr to a ThreadedIMClient::SharedInterfaces instance
//                           from this ThreadedIMClient object.
//==================================================================================================
void* ThreadedIMClient::monitorServerDirectives(void* threadArgs)
{
  boost::shared_ptr<ThreadedIMClient::SharedInterfaces> 
          sharedInterface(*(boost::shared_ptr<ThreadedIMClient::SharedInterfaces> *)threadArgs);
  while ( sharedInterface->checkKeepAlive() ) 
  {

    int select = sharedInterface->checkNetworkInput(1);
    if (select != 0) 
    {
      boost::shared_ptr<std::string> directive = sharedInterface->readFromServer();
      if ( (*directive).length() == 0 )
      {
        //Server connection dropped out underneath the client, handle gracefully!
        sharedInterface->disconnectFromServer();
        sharedInterface->printToConsole("-Connection to IM server has been terminated.");
        //Console monitoring run loop won't know to terminate the network thread, so we do it here
        ::pthread_exit(NULL);
      }
      try
      {
        Directive::Id id = Directive::Base::parseId(*directive);
        //Parse Connect Response
        if (id == Directive::ConnectResponseId)
        {
          Directive::ConnectResponse response;
          response.fromJson(*directive);
          boost::uint32_t responseCode = response.getCode();
          switch(responseCode)
          {
            case (Directive::ConnectResponse::Success):
            {
              sharedInterface->printToConsole("-Connected.");
              break;
            }
            case (Directive::ConnectResponse::NameTaken):
            {
              sharedInterface->printToConsole("-A user with that name already exists, connection failed.");
              sharedInterface->disconnectFromServer();
              //Console monitoring run loop won't know to terminate the network thread, so we do it here
              ::pthread_exit(NULL);
            }
            case (Directive::ConnectResponse::OtherError):
            default:
            {
              sharedInterface->printToConsole("-Connection failed to chat server.");
              sharedInterface->disconnectFromServer();
              //Console monitoring run loop won't know to terminate the network thread, so we do it here
              ::pthread_exit(NULL);
            }
          }
        }
        //Parse User Connected
        else if (id == Directive::UserConnectedId)
        {
          Directive::UserConnected response;
          response.fromJson(*directive);
          std::string newUserName = response.getName();
          sharedInterface->printToConsole("-" + newUserName + " has connected.");
        }
        //Parse User Disconnected
        else if (id == Directive::UserDisconnectedId)
        {
          Directive::UserDisconnected response;
          response.fromJson(*directive);
          std::string removedUserName = response.getName();
          sharedInterface->printToConsole("-" + removedUserName + " has disconnected.");
          sharedInterface->printToConsole("-" + removedUserName + " will be removed from @ and @@ commands...");
          sharedInterface->removeFromLastSentUsers(removedUserName);
          if ( removedUserName.compare( sharedInterface->checkLastReceivedUser() ) == 0 )
          {
            sharedInterface->updateLastReceivedUser("");
          }
        }
        //Parse Send IM
        else if (id == Directive::SendImId)
        {
          Directive::SendIm response;
          response.fromJson(*directive);
          std::string sender = response.getOrigin();
          std::string message = response.getContent();
          sharedInterface->printToConsole("->" + sender + " " + message);
          sharedInterface->updateLastReceivedUser(sender);
        }
        //Parse IM Failed
        else if (id == Directive::ImFailedId)
        {
          Directive::ImFailed response;
          response.fromJson(*directive);
          std::string badRecipient = response.getTarget();
          sharedInterface->printToConsole("-Failed to send IM to user: " + badRecipient);
          sharedInterface->printToConsole("-" + badRecipient + " will be removed from @ command...");
          sharedInterface->removeFromLastSentUsers(badRecipient);
        }
        //Parse List Response
        else if (id == Directive::ListResponseId)
        {
          Directive::ListResponse response;
          response.fromJson(*directive);
          const std::vector<std::string>& usersInResponse = response.getUsers();
          Lock::Scoped printUserListLock(sharedInterface->consoleIoLock);
          std::cout << "-Users currently connected:" << std::endl;
          std::stringstream userString;
          for (std::vector<std::string>::const_iterator userListIter = usersInResponse.begin(); 
               userListIter != usersInResponse.end(); userListIter++)
          {
            userString << "--" << *userListIter << "\n";
          }
          std::cout << userString.str() << std::endl;
        }
        else
        {
          sharedInterface->printToConsole("ERROR:  Received invalid json '" + *directive + "'...");
        }
      }
      catch (json::value_expected& e)
      {
        sharedInterface->printToConsole("ERROR:  Received invalid json '" + *directive + "'...");
      }
    }

  }
  ::pthread_exit(NULL);
}

//==================================================================================================
///   Launches a separate thread for monitoring network traffic from IM Server.
//==================================================================================================
void ThreadedIMClient::spawnMonitoringThread()
{
  this->sharedInterfaces->enableKeepAlive();
  ::pthread_create( &networkThreadId, NULL, monitorServerDirectives, 
                    (void*)&this->sharedInterfaces );
}

//==================================================================================================
///   Stops and cleans up the spawned separate thread for monitoring network traffic from IM Server.
//==================================================================================================
void ThreadedIMClient::cleanUpMonitoringThread()
{
  this->sharedInterfaces->disableKeepAlive();
  ::pthread_join(this->networkThreadId, NULL);
  this->networkThreadId = -1;
  if ( this->sharedInterfaces->isConnectedToServer() )
  {
    this->sharedInterfaces->disconnectFromServer();
  }
}

//==================================================================================================
///   Updates the client local user name value.
//    @param  newUserName     The string representing the new user name.
//==================================================================================================
void ThreadedIMClient::updateMyUserName(const std::string& newUserName)
{
  this->myUserName.reset( new std::string(newUserName) );
}

//==================================================================================================
///   Returns the client local user name value.
//    @return The current value stored as the local client user name (may be empty).
//==================================================================================================
const std::string& ThreadedIMClient::getMyUserName() const
{
  return *(this->myUserName);
}

//==================================================================================================
///   Maps an input candidate command do a command_type enum value.
//    @param  commandName     The string representing everything input from console before the
//                            first space character.
//    @return ThreadedIMClient::command_type enum value (CMD_NONE if input string does not match a
//            valid command)
//==================================================================================================
ThreadedIMClient::command_type ThreadedIMClient::returnCommandType(const std::string& inputLine)
{
  if ( boost::iequals(inputLine, "/connect") )
  {
    return CMD_CONNECT;
  } 
  else if ( boost::iequals(inputLine, "/disconnect") ) 
  {
    return CMD_DISCONNECT;
  } 
  else if ( boost::iequals(inputLine, "/show") ) 
  {
    return CMD_SHOW;
  } 
  else if ( boost::iequals(inputLine, "/help") ) 
  {
    return CMD_HELP;
  }
  else if ( boost::iequals(inputLine, "/quit") )
  {
    return CMD_QUIT;
  }
  else if ( (inputLine.length() > 1) && (inputLine.at(1) != '@') && (inputLine.at(0) == '@') )
  {
    return CMD_DIRECT_IM;
  }
  else if ( boost::iequals(inputLine, "@") ) 
  {
    return CMD_LAST_SENT_IM;
  }
  else if (boost::iequals(inputLine, "@@") )
  {
    return CMD_LAST_RECEIVED_IM;
  } 
  else
  {
    return CMD_NONE;
  }
}

//==================================================================================================
///   Processes /connect <userName>@<host>:<port> command from user.
//    @param  argument     The <userName>@<host>:<port> string for the connection.
//==================================================================================================
void ThreadedIMClient::processConnectCommand(const std::string& argument)
{
  if ( this->sharedInterfaces->isConnectedToServer() )
  {
    this->sharedInterfaces->printToConsole(
         "ERROR:  You are currently connected to a server, use /disconnect first...");
  }
  else
  {
    //Parse the input argument, ensure it's got a valid format
    std::string userName, hostName, portStr;
    int port;
    size_t atLoc = argument.find("@");
    size_t colonLoc = argument.find(":");
    if (atLoc == std::string::npos || colonLoc == std::string::npos) 
    {
      this->sharedInterfaces->printToConsole(
          "ERROR:  Invalid argument to /connect, see /help for details...");
      return;
    }
    userName = argument.substr(0, atLoc);
    hostName = argument.substr(atLoc+1, colonLoc-atLoc-1);
    portStr = argument.substr(colonLoc+1, std::string::npos);
    port = ::strtol(portStr.c_str(), NULL, 10);
    if (port == 0L || port == LONG_MAX || port == LONG_MIN)
    {
      this->sharedInterfaces->printToConsole(
          "ERROR:  Invalid argument to /connect, see /help for details...");
      return;
    }
    if (port < 1024 || port > 65535)
    {
      this->sharedInterfaces->printToConsole(
          "ERROR:  Invalid port number specified, must be between 1024 and 65535...");
      return;
    }

    //Attempt to connect to host:port
    try
    {
      this->sharedInterfaces->printToConsole("-Connecting to chat server...");
      this->sharedInterfaces->connectToServer(hostName, port);
    }
    catch (std::runtime_error& e)
    {
      this->sharedInterfaces->printToConsole(
          "ERROR:  Unable to connect to " + hostName + ":" + portStr + "...");
      //The socket still will think it's connected unless we...
      this->sharedInterfaces->disconnectFromServer();
      return;
    }

    //Spawn the network monitoring thread
    this->spawnMonitoringThread();

    //Send Connect directive to server
    Directive::Connect connectDirective(userName);
    std::string directiveJson = connectDirective.toJson();
    directiveJson.append("\r\n");
    this->sharedInterfaces->writeToServer(directiveJson);

    //Update local user name
    this->updateMyUserName(userName);
  }
}

//==================================================================================================
///   Processes /disconnect command input from user.
//==================================================================================================
void ThreadedIMClient::processDisconnectCommand()
{
  if ( this->sharedInterfaces->isConnectedToServer() )
  {
    //Send Disconnect directive to server
    Directive::Disconnect disconnectDirective;
    std::string directiveJson = disconnectDirective.toJson();
    directiveJson.append("\r\n");
    this->sharedInterfaces->writeToServer(directiveJson);

    //Stop the network monitoring thread
    this->cleanUpMonitoringThread();

    //Disconnect from the server
    if ( this->sharedInterfaces->isConnectedToServer() )
    {
      this->sharedInterfaces->disconnectFromServer();
    }
  }
  else
  {
    this->sharedInterfaces->printToConsole(
          "ERROR:  You are not currently connected to a server, ignoring...");
  }
}

//==================================================================================================
///   Processes /show command input from user.
//==================================================================================================
void ThreadedIMClient::processShowCommand()
{
  if ( this->sharedInterfaces->isConnectedToServer() )
  {
    //Send List Request directive to server
    Directive::ListRequest listRequest;
    std::string directiveJson = listRequest.toJson();
    directiveJson.append("\r\n");
    this->sharedInterfaces->writeToServer(directiveJson);
  }
  else
  {
    this->sharedInterfaces->printToConsole(
        "ERROR:  You are not currently connected to a server, ignoring...");
  }
}

//==================================================================================================
///   Processes /help command input from user.
//==================================================================================================
void ThreadedIMClient::processHelpCommand()
{
  Lock::Scoped lotsToPrintLock(this->sharedInterfaces->consoleIoLock);
  std::cout << "Valid commands:" << std::endl;
  std::cout << "/connect <userName>@<host>:<port>   # Connect to server <host>:<port> as <userName>"
            << std::endl;
  std::cout << "/disconnect                         # Disconnect from current server" << std::endl;
  std::cout << "/show                               # Show all connected users" << std::endl;
  std::cout << "/help                               # Print this help command list" << std::endl;
  std::cout << "/quit                               # Disconnect from current server and quit program"
            << std::endl;
  std::cout << "@user1[,user2,...,userN] <message>  # Send <message> to user1,user2,...,userN"
            << std::endl;
  std::cout << "@ <message>                         # Send <message> to recipient of last outgoing message"
            << std::endl;
  std::cout << "@@ <message>                        # Send <message> to sender of last incoming message"
            << std::endl;
  std::cout << std::endl;
}

//==================================================================================================
///   Processes /quit command input from user.
//==================================================================================================
void ThreadedIMClient::processQuitCommand()
{
  if ( this->sharedInterfaces->isConnectedToServer() )
  {
    //First handle the spawned network monitoring thread if connected
    this->processDisconnectCommand();
  }
  //Then cause the console run loop to terminate
  this->getConsoleInput = false;
  this->sharedInterfaces->printToConsole("Quitting imClient...");
}

//==================================================================================================
///   Processes @, @@, @user1[,user2,...,userN] input commands input from the user.
//    @param  command     The command to parse (one of @, @@, or @user1[,user2,...,userN]
//    @param  message     The message to be sent to the appropriate user(s).
//==================================================================================================
void ThreadedIMClient::processSendIMCommand(const std::string& command, const std::string& message)
{
  //Verify we have a valid message
  if ( message.empty() )
  {
    this->sharedInterfaces->printToConsole(
        "ERROR:  You must type a message to be sent to other users, ignoring...");
    return;
  }
  //Verify we are connected to a server
  else if ( !this->sharedInterfaces->isConnectedToServer() )
  {
    this->sharedInterfaces->printToConsole(
        "ERROR:  You are not currently connected to a server, ignoring...");
    return;
  }
  else
  {
    //Validate list of users to send message to, and update if changing
    if (command.length() > 1 && command.at(0) == '@' && command.at(1) == '@')
    {
      //Verify the last received user is populated
      std::string lastReceivedUser = this->sharedInterfaces->checkLastReceivedUser();
      if ( lastReceivedUser.empty() )
      {
        this->sharedInterfaces->printToConsole(
            "ERROR:  You cannot use this command until you first receive a message, ignoring...");
        return;
      }
      else
      {
        this->sharedInterfaces->clearLastSentUsers();
        this->sharedInterfaces->addToLastSentUsers(lastReceivedUser);
      }
    }
    else if (command.length() == 1 && command.at(0) == '@')
    {
      //Verify the last sent users is populated
      if ( this->sharedInterfaces->getLastSentUsers().empty() )
      {
        this->sharedInterfaces->printToConsole(
            "ERROR:  You cannot use this command until you first send a direct message to user(s), ignoring...");
        return;
      }
    }
    else if (command.length() > 1 && command.at(0) == '@' && command.at(1) != '@')
    {
      //Tokenize the user list, put into last sent users
      this->sharedInterfaces->clearLastSentUsers();
      std::string userList = command.substr(1, std::string::npos);
      boost::char_separator<char> delim(",");
      boost::tokenizer<boost::char_separator<char>/**/> tokens(userList, delim);
      BOOST_FOREACH (const std::string& userName, tokens)
      {
        this->sharedInterfaces->addToLastSentUsers(userName);
      }
    }
    else
    {
      //Shouldn't get here but just incase...
      this->sharedInterfaces->printToConsole(
          "ERROR:  Illegal argument to @, see /help for details");
    }

    //Form the SendIM directive from the lastSentUsers list, and send
    const std::vector<std::string>& lastSentUsers = this->sharedInterfaces->getLastSentUsers();
    for (std::vector<std::string>::const_iterator iter = lastSentUsers.begin();
         iter != lastSentUsers.end();
         iter++)
    {
      Directive::ImData imData;
      imData.origin = this->getMyUserName();
      imData.target = *iter;
      imData.content = message;
      Directive::SendIm sendIm(imData);
      std::string directiveJson = sendIm.toJson();
      directiveJson.append("\r\n");
      this->sharedInterfaces->writeToServer(directiveJson);
      //I've seen periodically when sending to multiple users, more than one Send IM received at
      //once on the server, it isn't processed ok.  So, trying to space out the sent directives...
      ::usleep(100);
    }
  }
}

//==================================================================================================
///   Process an input line from the console as a command, and act accordingly.
//    @param  inputLine     The line representing what was input from the console to be parsed
//==================================================================================================
void ThreadedIMClient::processConsoleInput(const std::string& inputLine)
{
  //Split into <Command>, <Arguments> (split on first space char)
  std::string commandName, commandArgs;
  size_t foundSpace = inputLine.find_first_of(" ");
  if (foundSpace != std::string::npos)
  {
    commandName = inputLine.substr(0, foundSpace);
    commandArgs = inputLine.substr(foundSpace+1, std::string::npos);
  }
  else
  {
    commandName = inputLine;
    commandArgs = "";
  }
  //Determine if valid command (/connect, /disconnect, /show, /help, /quit, @, @@)
  ThreadedIMClient::command_type whatCommand = this->returnCommandType(commandName);
  switch (whatCommand) 
  {
    case CMD_CONNECT: 
    {
      if ( commandArgs.empty() )
      {
        this->sharedInterfaces->printToConsole(
            "ERROR:  you must supply a valid <userName>@<host>:<port> argument, see /help for details");
      }
      else
      {
        this->processConnectCommand(commandArgs);
      }
      break;
    }
    case CMD_DISCONNECT: 
    {
      if ( commandArgs.empty() )
      {
        this->processDisconnectCommand();
      }
      else
      {
        this->sharedInterfaces->printToConsole("ERROR:  /disconnect takes no arguments, see /help for details");
      }
      break;
    }
    case CMD_SHOW:
    {
      if ( commandArgs.empty() )
      {
        this->processShowCommand();
      }
      else
      {
        this->sharedInterfaces->printToConsole("ERROR:  /show takes no arguments, see /help for details");
      }
      break;
    }
    case CMD_HELP:
    {
      //...Don't enforce no args check here; informing them to see /help when they're trying
      //to do that already seems wrong, just print out help and ignore the passed argument(s)
      this->processHelpCommand();
      break;
    }
    case CMD_QUIT:
    {
      if ( commandArgs.empty() )
      {
        this->processQuitCommand();
      }
      else
      {
        this->sharedInterfaces->printToConsole("ERROR:  /quit takes no arguments, see /help for details");
      }
      break;
    }
    case CMD_DIRECT_IM:
    case CMD_LAST_SENT_IM:
    case CMD_LAST_RECEIVED_IM:
    {
      if ( commandArgs.empty() )
      {
        this->sharedInterfaces->printToConsole(
            "ERROR:  you must supply a valid message to send to other user(s), see /help for details");
      }
      else
      {
        this->processSendIMCommand(commandName, commandArgs);
      }
      break;
    }
    case CMD_NONE:
    default:
    {
      this->sharedInterfaces->printToConsole("ERROR:  invalid command, see /help for details");
    }
  }
}

//==================================================================================================
///   Run loop for monitoring and interpreting user actions from stdin.  Will trigger Directives
//    to be sent to the IM Server if necessary.
//==================================================================================================
void ThreadedIMClient::runConsoleLoop()
{
  while (this->getConsoleInput) 
  {
    //handle a cleanup event incase server connection pulled underneath, rejoin the network thread
    if ( this->networkThreadId != -1 && !this->sharedInterfaces->isConnectedToServer() )
    {
      this->cleanUpMonitoringThread();
    }
    int consoleInput = this->sharedInterfaces->checkConsoleInput(1);
    if (consoleInput != 0) 
    {
      std::string inputLine;
      std::getline(std::cin, inputLine);
      std::cout << std::endl;
      this->processConsoleInput(inputLine);
    }
  }
}
