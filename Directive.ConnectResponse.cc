//==================================================================================================
//    Directive.ConnectResponse.cc
//==================================================================================================
#include "Directive.ConnectResponse.h"

namespace Directive
{
  //================================================================================================
  ///   Default constructor.
  //================================================================================================
  ConnectResponse::ConnectResponse()
  {
  }

  //================================================================================================
  ///   Useful constructor.
  //================================================================================================
  ConnectResponse::ConnectResponse(boost::uint32_t code, const std::string& name)
  : code(code), name(name)
  {
  }

  //================================================================================================
  ///   Destructor.
  //================================================================================================
  ConnectResponse::~ConnectResponse()
  {
  }

  //================================================================================================
  ///   Converts to json.
  //================================================================================================
  std::string ConnectResponse::toJson() const
  {
    json::object o;
    o.append(Directive::Base::IdKey, Directive::asStr(this->id()));
    o.append(Directive::ConnectResponse::CodeKey, Directive::asStr(this->code));
    o.append(Directive::ConnectResponse::NameKey, this->name);
    return json::encode(o);
  }

  //================================================================================================
  ///   Converts from json.
  //================================================================================================
  void ConnectResponse::fromJson(const std::string& data)
  {
    json::value j = json::parse(data);
    if (j.is_object())
    {
      if (!j.has_key(Directive::ConnectResponse::CodeKey) ||
          !j.has_key(Directive::ConnectResponse::NameKey))
      {
        return;
      }

      json::value code = j[Directive::ConnectResponse::CodeKey];
      if (code.is_string())
      {
        this->code = Directive::asInt(code.to_string());
      }

      json::value name = j[Directive::ConnectResponse::NameKey];
      if (name.is_string())
      {
        this->name = name.to_string();
      }
    }
  }

  //================================================================================================
  ///   Returns the id.
  //================================================================================================
  Directive::Id ConnectResponse::id() const
  {
    return Directive::ConnectResponseId;
  }

  //================================================================================================
  ///   Returns the response code from the connect response.
  //================================================================================================
  boost::uint32_t ConnectResponse::getCode() const
  {
    return this->code;
  }

  //================================================================================================
  ///   Returns the user name that triggered the response.
  //================================================================================================
  const std::string& ConnectResponse::getName() const
  {
    return this->name;
  }
};
