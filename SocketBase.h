//==================================================================================================
//    SocketBase.h
//==================================================================================================
#ifndef SOCKET_BASE_H
#define SOCKET_BASE_H
#include <boost/shared_ptr.hpp>
#include <string>
#include <sys/socket.h>
#include <sys/time.h>
#define OneMB 1024 * 1024
//==================================================================================================
///   Abstract base class for socket connections.
//==================================================================================================
class SocketBase
{
public:
  typedef unsigned int SocketType;
  static const SocketType Tcp = SOCK_STREAM;
  static const SocketType Udp = SOCK_DGRAM;
               
  static const int BufferSize = OneMB;

public:
  SocketBase(SocketType type, bool CloseOnDestruct=true);
  virtual ~SocketBase();

  virtual void connect() = 0;
  void disconnect(bool strict=false);
  bool isConnected() const;
  int socketId() const;

  boost::shared_ptr<std::string> read(bool toEof=false) const;
  void write(const std::string& data) const;

  static int selectReadOnly(int socketId, int timeoutSec); 

protected:
  SocketType type;
  int socket;  
  bool closeOnDestruct;
};

#endif // SOCKET_BASE_H
