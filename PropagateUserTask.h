//==================================================================================================
//    PropagateUserTask.h
//==================================================================================================
#ifndef PROPAGATE_USER_TASK_H
#define PROPAGATE_USER_TASK_H
#include <boost/shared_ptr.hpp>
#include "Directive.PropagateUser.h"
#include "ServerState.h"
#include "SocketThread.h"
#include "TaskPool.Task.h"

//==================================================================================================
///   Handles the work required to accept a new user from a peer and relay it forward.
//==================================================================================================
class PropagateUserTask : public TaskPool::Task
{
public:
  PropagateUserTask(const Directive::PropagateUser& directive,
                    const boost::shared_ptr<SocketThread>& socketThread,
                    const boost::shared_ptr<ServerState>& serverState);
  virtual ~PropagateUserTask();

  void process();

protected:
  Directive::PropagateUser directive;
  boost::shared_ptr<SocketThread> socketThread;
  boost::shared_ptr<ServerState> serverState;
};

#endif // PROPAGATE_USER_TASK_H
