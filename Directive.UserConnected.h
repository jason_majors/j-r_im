//==================================================================================================
//    Directive.UserConnected.h
//==================================================================================================
#ifndef DIRECTIVE_USER_CONNECTED_H
#define DIRECTIVE_USER_CONNECTED_H
#include "Directive.Base.h"
namespace Directive
{
  class UserConnected : public Directive::Base
  {
  public:
    UserConnected();
    UserConnected(const std::string& name);
    virtual ~UserConnected();
    std::string toJson() const;
    void fromJson(const std::string& data);
    Directive::Id id() const;
    const std::string& getName() const;

  protected:
    std::string name;
    const std::string UserConnectedKey = "UserConnected";
  };
};

#endif // DIRECTIVE_USER_CONNECTED_H
