//==================================================================================================
//    SubscribeTask.cc
//==================================================================================================
#include "Exceptions.h"
#include "Logger.h"
#include "SubscribeTask.h"
#include "Directive.SubscribeResponse.h"

static const std::string CRLF = "\r\n";
//==================================================================================================
///   Constructor.
//==================================================================================================
SubscribeTask::SubscribeTask(const Directive::Subscribe& directive,
                         const boost::shared_ptr<SocketThread>& socketThread,
                         const boost::shared_ptr<ServerState>& serverState)
: directive(directive), socketThread(socketThread), serverState(serverState)
{
}

//==================================================================================================
///   Destructor.
//==================================================================================================
SubscribeTask::~SubscribeTask()
{
}

//==================================================================================================
///   Called when the task starts.
//==================================================================================================
void SubscribeTask::process()
{
  try
  {
    Logger::getLogger()->info("SubscribeTask: " + this->directive.toJson());
    ServerState::ServerInfo info;
    info.first = this->socketThread;
    Directive::Server server(this->directive.getServer());
    info.second = server;
    this->serverState->addChild(info);

    Directive::SubscribeResponse response(Directive::SubscribeResponse::Success,
                                          *(this->serverState->getUserNames()));
    this->socketThread->write(response.toJson() + CRLF);
  }
  catch(Exceptions::WriteException& e)
  {
    Logger::getLogger()->error("SubscribeTask: Caught WriteException:");
    Logger::getLogger()->error(e.what());
  }
}
